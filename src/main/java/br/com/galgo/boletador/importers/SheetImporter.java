/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.galgo.boletador.importers;

import com.sistemagalgo.schemaposicaoativos.GalgoAssBalStmtComplexType;
import org.apache.poi.ss.usermodel.Sheet;

/**
 * Classe que deve ser implementada pelas classes importadoras de abas
 *
 * @author valdemar.arantes
 */
abstract class SheetImporter {

    final Sheet sheet;
    final GalgoAssBalStmtComplexType glg;

    /**
     *
     * @param glg Representação JAXB da Posição de Ativos a ser atualizada
     * @param sheet Aba a ser importada para atualizar o campo glg
     */
    public SheetImporter(GalgoAssBalStmtComplexType glg, Sheet sheet) {
        this.glg = glg;
        this.sheet = sheet;
    }

    /**
     * Importa os dados da aba, atualizando a representação DOM do XML
     */
    public abstract void importSheet();


}
