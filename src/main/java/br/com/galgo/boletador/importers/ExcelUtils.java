/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.galgo.boletador.importers;

import br.com.galgo.boletador.importers.converter.StringToXMLGregorianCalendarConverter;
import com.galgo.utils.ApplicationException;
import com.galgo.utils.XMLGregorianCalendarConversionUtil;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import java.text.ParseException;
import java.util.List;
import java.util.Map;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.XMLGregorianCalendar;
import org.apache.commons.lang.NullArgumentException;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author valdemar.arantes
 */
public class ExcelUtils {

    private static final Logger log = LoggerFactory.getLogger(ExcelUtils.class);

    /**
     * Monta o Map usando os valores da linha de Header como chaves do mapa e os valores da linha
     * abaixo como valores do mapa, todos os valores retornados são String.
     *
     * @param sheet
     * @param hdrRowNr
     * @param dataRowNr
     * @return
     */
    public static Map<String, Object> buildBaseMap(Sheet sheet, int hdrRowNr, int dataRowNr) {
        Row hdrRow = sheet.getRow(hdrRowNr);
        Row dataRow = sheet.getRow(dataRowNr);

        short hdrLastCellNum = hdrRow.getLastCellNum();
        short dataLastCellNum = dataRow.getLastCellNum();
        log.debug("hdrLastCellNum={}; dataLastCellNum={}", hdrLastCellNum, dataLastCellNum);

        Map<String, Object> map = Maps.newHashMap();
        for (int col = 0; col < hdrLastCellNum; col++) {
            String data = getCellAsString(dataRow, col);
            String hdr = getCellAsString(hdrRow, col);
            map.put(hdr, data);
        }
        return map;
    }

    /**
     * Retorna o conteúdo de uma célula como String. Se a linha não existir, lança a exceção
     * NonExistentRowException. Se a célula não for nem string nem numérica, lança a exceção
     * ApplicationException.
     *
     * @param sheet
     * @param rowNr
     * @param colNr
     * @return
     */
    public static String getCellAsString(Sheet sheet, int rowNr, int colNr) {
        Row row = sheet.getRow(rowNr);
        if (row == null) {
            throw new NonExistentRowException();
        }

        return getCellAsString(row, colNr);
    }

    /**
     * Retorna o conteúdo de uma célula como String. Se a célula não for nem string nem numérica,
     * lança a exceção ApplicationException.
     *
     * @param row
     * @param colNr
     * @return
     */
    public static String getCellAsString(Row row, int colNr) {
        Cell cell = row.getCell(colNr, Row.RETURN_BLANK_AS_NULL);
        if (cell == null) {
            return null;
        }
        switch (cell.getCellType()) {
            case Cell.CELL_TYPE_NUMERIC:
                double value = cell.getNumericCellValue();
                return "" + value;
            case Cell.CELL_TYPE_STRING:
                return cell.getStringCellValue();
            default:
                throw new ApplicationException("Dado da linha=" + row.getRowNum() + " e col="
                        + colNr
                        + " com tipo=" + cell.getCellType() + " não mapeado");
        }
    }

    public static XMLGregorianCalendar getCellAsXMLGregCalendarAAAAMMDDTZ(String strDate) {
        try {
            log.debug("dtCriacao={}", strDate);
            XMLGregorianCalendar tipoDtInf = StringToXMLGregorianCalendarConverter.
                    stringToXMLGregorianCalendarTimeZone(strDate);
            tipoDtInf = tipoDtInf.normalize();
            log.debug("Data Criação = " + tipoDtInf.toString());
            return tipoDtInf;

        } catch (ParseException ex) {
            log.debug("Data Criação = " + strDate);
            System.out.println("Erro no Parse");
        } catch (DatatypeConfigurationException ex) {
        }
        return null;

    }

    /**
     * Obtém uma data no formato XMLGregorianCalendar no formato AAAAMMDD
     *
     * @param strDate
     * @return
     */
    public static XMLGregorianCalendar getCellAsXMLGregCalendarAAAAMMDD(String strDate) {
        try {
            log.debug("Convertendo {} para o formato XMLGregorianCalendar...", strDate);
            XMLGregorianCalendar tipoDtInf = XMLGregorianCalendarConversionUtil.
                    asXMLGregorianCalendar_Date(strDate, "yyyy-MM-dd");
            log.debug("Data convertida = " + tipoDtInf.toString());
            return tipoDtInf;

        } catch (ParseException ex) {
            log.debug("Data Criação = " + strDate);
            System.out.println("Erro no Parse");
        }
        return null;

    }

    /**
     * Monta uma lista de Maps utilizando initialRowNr como sendo a primeira linha de uma série de
     * linhas e o intervalo de colunas definido entre initialColNr e finalColNr inclusive. A leitura
     * das linhas para quando ou a linha não está definida ou quando todas as células do intervalo
     * de uma linha são nulas. A primeira linha é considerada como sendo a Header Line.
     *
     * @param sheet
     * @param initialRowNr
     * @param initialColNr
     * @param finalColNr
     * @return
     */
    public static List<Map<String, String>> getCellsAsListOfMap(Sheet sheet, int initialRowNr,
            int initialColNr, int finalColNr) {
        List<Map<String, String>> list = Lists.newArrayList();
        int rowNr = initialRowNr + 1;

        // Recuperando o cabeçalho como uma lista de Strings
        Row hdrRow = sheet.getRow(initialRowNr);
        List<String> hdrList = Lists.newArrayList();
        for (int col = initialColNr; col <= finalColNr; col++) {
            hdrList.add(getCellAsString(hdrRow, col));
        }

        // Montando a lista de Maps
        do {
            Row row = sheet.getRow(rowNr);
            if (row == null) {
                break;
            }
            Map<String, String> map = Maps.newHashMap();
            boolean allCellsAreNull = true;
            for (int col = initialColNr; col <= finalColNr; col++) {
                try {
                    String cellValue = StringUtils.trimToNull(getCellAsString(row, col));
                    if (cellValue != null) {
                        allCellsAreNull = false;
                    }
                    map.put(hdrList.get(col - initialColNr), cellValue);
                } catch (Exception e) {
                    log.error("Erro ao ler célular[{},{}]", rowNr, col);
                    log.error(null, e);
                    continue;
                }
            }
            if (allCellsAreNull) {
                break;
            } else {
                list.add(map);
                rowNr++;
            }
        } while (true);
        return list;
    }

    /**
     * Retorna o nome da colua: Coluna 0 (zero) = A
     *
     * @param columnNumber Numeração começa em 0 (zero)
     * @return
     */
    public static String getExcelColumnName(int columnNumber) {
        int dividend = columnNumber + 1;
        int i;
        String columnName = "";
        int modulo;
        while (dividend > 0) {
            modulo = (dividend - 1) % 26;
            i = 65 + modulo;
            columnName = new Character((char) i).toString() + columnName;
            dividend = (int) ((dividend - modulo) / 26);
        }
        return columnName;
    }

    /**
     * Verifica se uma célula é nul, possui brancos ou um String vazia
     *
     * @param sheet Planilha a ser verificada. Se argumento for nulo, lança a Runtime Exception
     * NullArgumentException
     * @param rowNr Linha da planilha a ser verificada.
     * @param colNr Coluna da planilha a ser verificada.
     *
     * @return true se a celula da planilha possuir brancos, "" (string vazia), for nula ou se a
     * linha for nula.
     */
    public static boolean isBlank(Sheet sheet, int rowNr, int colNr) {
        if (sheet == null) {
            throw new NullArgumentException("sheet");
        }

        String cellAsString = getCellAsString(sheet, rowNr, colNr);
        return StringUtils.isBlank(cellAsString);
    }

    /**
     * Verifica se um intervalos de células possui alguma célula preenchida
     *
     * @param sheet planilha sob teste
     * @param rowNr linha desejada
     * @param firstCol primeira coluna do intervalo
     * @param lastCol última coluna do intervalo
     * @return true se todas as células do intervalo estiverem vazias
     */
    public static boolean isBlank(Sheet sheet, int rowNr, int firstCol, int lastCol) {
        Row row = sheet.getRow(rowNr);
        if (row == null) {
            log.info("Linha {} da planilha não encontrada", rowNr);
            return true;
        }
        for (int col = firstCol; col < lastCol; col++) {
            Cell cell = row.getCell(col);
            if (cell != null && (cell.getCellType() != Cell.CELL_TYPE_BLANK)) {
                return false;
            }
        }
        log.info("Não foram encontrado dados na planilha nas colunas de {} a {} da linha {}",
                firstCol, lastCol, rowNr);
        return true;
    }
}
