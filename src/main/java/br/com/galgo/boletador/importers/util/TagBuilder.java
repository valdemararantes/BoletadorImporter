/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.galgo.boletador.importers.util;

import br.com.galgo.boletador.importers.ExcelUtils;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import org.apache.poi.ss.usermodel.Sheet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author valdemar.arantes
 */
class TagBuilder<T> {

    private static final Logger log = LoggerFactory.getLogger(TagBuilder.class);
    protected final Sheet sheet;
    protected final int sheetCol;
    protected final int sheetLine;
    protected T element;
    private final int size;

    protected TagBuilder(Sheet sheet, int sheetLine, int sheetCol, int size) {
        this.sheet = sheet;
        this.sheetCol = sheetCol;
        this.sheetLine = sheetLine;
        this.size = size;

        if (!ExcelUtils.isBlank(sheet, sheetLine, sheetCol, sheetCol + size)) {
            try {
                Type type = this.getClass().getGenericSuperclass();
                Type[] typeArgs = ((ParameterizedType) type).getActualTypeArguments();
                element = (T) ((Class) typeArgs[0]).newInstance();
            } catch (InstantiationException | IllegalAccessException e) {
                log.error(null, e);
            }
        }
    }

}
