/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.galgo.boletador.importers.util;

import br.anbimacom.balanceforsubaccountbrazil.BalForSubAcctBrData;
import br.com.galgo.boletador.importers.ExcelUtils;
import org.apache.poi.ss.usermodel.Sheet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

//import br.com.galgo.boletador.importers.ExcelUtils;

/**
 *
 * @author valdemar.arantes
 */
class RealStatePrtflSupplData extends TagBuilder<BalForSubAcctBrData.RealStatePrtfl> {

    private static final Logger log = LoggerFactory.getLogger(SwapSupplData.class);
    public static final int SIZE = 8;

    RealStatePrtflSupplData(Sheet sheet, int sheetLine, int sheetCol) {
        super(sheet, sheetLine, sheetCol, SIZE);
    }

    BalForSubAcctBrData.RealStatePrtfl getRealStatePrtfl() {
        if (element == null) {
            return null;
        }

        populate();
        return element;
    }

    private void populate() {
        // NameAndAddress <NmAndAdr> / <Nm> [0..1]
        int actualCol = sheetCol;
        JaxbBeanFiller.fill(sheet, sheetLine, sheetCol, "nm", element, "nmAndAdr");

        // NameAndAddress <NmAndAdr> / <Adr> [0..1]
        actualCol++;
        if (!ExcelUtils.isBlank(sheet, sheetLine, actualCol, actualCol + 7)) {
            int localActualCol = actualCol;
            String[] props = {"strtNm", "bldgNb", "flr", "pstCd", "twnNm", "ctrySubDvsn", "ctry"};
            for (String prop : props) {
                JaxbBeanFiller.fill(sheet, sheetLine, localActualCol, prop, element.getNmAndAdr(), "adr");
                localActualCol++;
            }
        }

        // Fund's Participation in property <FundPartProperty> [1..1]
        actualCol += 7;
        JaxbBeanFiller.fill(sheet, sheetLine, actualCol, null, element, "fundPartProperty");

        // Rationale for Book Value / Code
        actualCol++;
        JaxbBeanFiller.fill(sheet, sheetLine, actualCol, "cd", element, "ratBookVal");

        // ValuationInformation <ValtInf> [0..1]
        actualCol++;
        element.setValtInf(new ValuationInformationSuppl
            (sheet, sheetLine, actualCol).getElement());

        // Lease <Lease> [0..1]
        actualCol += ValuationInformationSuppl.SIZE;
        element.getLease().add(new LeaseSupplData(sheet, sheetLine, actualCol).getElement());
//        element.getLease().getTp().setCd("string");
//        element.getLease().getAmt().setValue(BigDecimal.ZERO);
//        element.getLease().getAmt().setCcy("String");
    }
}
