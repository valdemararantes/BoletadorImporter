/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.galgo.boletador.importers.util;

import iso.std.iso._20022.tech.xsd.semt_003_001.ActiveOrHistoricCurrencyAnd13DecimalAmount;
import iso.std.iso._20022.tech.xsd.semt_003_001.Price2;
import iso.std.iso._20022.tech.xsd.semt_003_001.PriceRateOrAmountChoice;
import iso.std.iso._20022.tech.xsd.semt_003_001.PriceValueType1Code;
import iso.std.iso._20022.tech.xsd.semt_003_001.YieldedOrValueType1Choice;
import java.math.BigDecimal;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author joyce.oliveira
 */
public class SubscriptionPrice {

    private static final Logger log = LoggerFactory.getLogger(SubscriptionPrice.class);

    public void geraSubscriptionPrice(Price2 precoEmissao, String tipoPreco, String moeda,
            String valorPreco) {

        log.trace("precoEmissao={}, tipoPreco={}, ...", precoEmissao, tipoPreco);

        YieldedOrValueType1Choice tp = precoEmissao.getTp();
        if (tp == null) {
            tp = new YieldedOrValueType1Choice();
            precoEmissao.setTp(tp);
        }

        PriceValueType1Code tpPreco = PriceValueType1Code.fromValue(tipoPreco);
        tp.setValTp(tpPreco);

        PriceRateOrAmountChoice val = precoEmissao.getVal();
        if (val == null) {
            val = new PriceRateOrAmountChoice();
            precoEmissao.setVal(val);
        }

        ActiveOrHistoricCurrencyAnd13DecimalAmount amt = val.getAmt();
        if (amt == null) {
            amt = new ActiveOrHistoricCurrencyAnd13DecimalAmount();
            val.setAmt(amt);
        }

        amt.setCcy(moeda);

        BigDecimal vlrPrc = new BigDecimal(valorPreco);
        amt.setValue(vlrPrc);

    }
}
