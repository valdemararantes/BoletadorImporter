/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.galgo.boletador.importers.util;

import br.anbimacom.balanceforsubaccountbrazil.BalForSubAcctBrData;
import br.com.galgo.boletador.importers.ExcelUtils;
import com.galgo.utils.XMLGregorianCalendarConversionUtil;
import com.google.common.collect.Lists;
import java.beans.PropertyDescriptor;
import java.lang.reflect.InvocationTargetException;
import java.text.ParseException;
import java.util.List;
import javax.xml.datatype.XMLGregorianCalendar;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.beanutils.ConvertUtils;
import org.apache.commons.beanutils.ConvertUtilsBean;
import org.apache.commons.beanutils.Converter;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.ListUtils;
import org.apache.commons.lang.ObjectUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.reflect.ConstructorUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author valdemar.arantes
 */
public class JaxbBeanFiller {

    private static final Logger log = LoggerFactory.getLogger(JaxbBeanFiller.class);

    static {
        registerXMLGregorianCalendarConverter();
    }

    /**
     * Preenche dados Jaxb a partir da planilha
     *
     * @param sheet Planilha com os dados origem
     * @param sheetLine Linha inicial da planilha com os dados
     * @param actualCol Coluna inicial da planilha com os dados
     * @param propertyName propriedade do objeto da clase clazz que recebe os dados da planilha. Se
     * nulo, seta o valor da planilha direto na propriedade do parentObject
     * @param parentObject Objeto pai do ojeto instanciado do tipo clazz
     * @param parentPropertyName Propriedade do objeto parentObject que receberá a instância de
     * clazz
     */
    public static void fill(Sheet sheet, int sheetLine, int actualCol,
            String propertyName, Object parentObject, String parentPropertyName) {
        String excelValue = ExcelUtils.getCellAsString(sheet, sheetLine, actualCol);
        if (StringUtils.isNotBlank(excelValue)) {
            try {
                Object obj = PropertyUtils.getProperty(parentObject, parentPropertyName);
                if (propertyName != null) {
                    // Se a campo do objeto pai for nulo, instancio um
                    if (obj == null) {
                        PropertyDescriptor propDescr = PropertyUtils.getPropertyDescriptor(
                                parentObject,
                                parentPropertyName);
                        Class clazz = propDescr.getPropertyType();

                        obj = newInstance(clazz);
                        if (obj == null) {
                            return;
                        }
                        setProperty(parentObject, parentPropertyName, obj);
                    }

                    BeanUtils.setProperty(obj, propertyName, excelValue);

                } else {
                    BeanUtils.setProperty(parentObject, parentPropertyName, excelValue);
                }
            } catch (IllegalAccessException | InvocationTargetException |
                    NoSuchMethodException e) {
                log.error(null, e);
                throw new RuntimeException("Erro no aplicativo: " + e);
            }
        }
    }

    /**
     *
     * @param sheet Planilha com os dados origem
     * @param sheetLine Linha inicial da planilha com os dados
     * @param actualCol Coluna inicial da planilha com os dados
     * @param propertiesNames Propriedades do objeto do tipo clazz que recebe os dados da planilha
     * @param parentObject Objeto pai do ojeto instanciado do tipo clazz
     * @param parentPropertyName Propriedade do objeto parentObject que receberá a instância de
     * @param repeatable Indica se estes dados podem ser repetidos
     * @param refCol Em caso dos dados poerem ser repetidos, esta é a coluna que me mostra, se
     * estiver vazia, que os dados são uma repetição
     */
    public static void fill(Sheet sheet, int sheetLine, int actualCol,
            List<String> propertiesNames, Object parentObject, String parentPropertyName,
            boolean repeatable, int refCol) {

        if (CollectionUtils.isEmpty(propertiesNames)) {
            log.info("Lista de propriedades vazia. Retornando");
            return;
        }

        int actualLine = sheetLine - 1;
        do {
            actualLine++;
            List<String> sheetValues = readSheetValues(sheet, actualLine, actualCol,
                    propertiesNames.size());
            if (CollectionUtils.isEmpty(sheetValues)) {
                return;
            }

            try {
                PropertyDescriptor propDescr = PropertyUtils.getPropertyDescriptor(parentObject,
                        parentPropertyName);
                Class clazz = propDescr.getPropertyType();
                Object obj = newInstance(clazz);
                int index = 0;
                for (String propertyName : propertiesNames) {
                    BeanUtils.setProperty(obj, propertyName, sheetValues.get(index));
                    index++;
                }
                PropertyUtils.setProperty(parentObject, parentPropertyName, obj);
            } catch (IllegalAccessException | InvocationTargetException |
                    NoSuchMethodException e) {
                log.error(null, e);
                throw new RuntimeException("Erro no aplicativo: " + e.toString());
            }

            if (!repeatable) {
                break;
            }
            if (isRepetitionsEnd(sheet, actualLine, actualCol, refCol)) {
                break;
            }

        } while (true);
    }

    /**
     * Inidica se é o fim dos dados repetidos olhando a célula de referência da próxima linha
     *
     * @param sheet
     * @param line Última linha da planilha com os dados lidos
     * @param col Coluna onde se iniciam os dados a serem lidos
     * @param refCol Coluna de refeência que nos diz, quando vazia, que há novos dados de repetição
     * @return
     */
    private static boolean isRepetitionsEnd(Sheet sheet, int line, int col, int refCol) {
        int nextLine = line + 1;
        Row nextRow = sheet.getRow(nextLine);
        if (nextRow == null) {
            log.info("Linha {} não definida", nextLine);
            return true;
        }

        Cell refCell = nextRow.getCell(refCol);
        if (refCell == null || (refCell.getCellType() == Cell.CELL_TYPE_BLANK)) {
            /*
             Com a célula de refeência está vazia, então esta linha possui dados de repetição,
             logo, não é o fim
             */
            return false;
        }

        return true;
    }

    private static Object setProperty(Object obj, String propertyName,
            Object value) {
        try {
            PropertyUtils.setProperty(obj, propertyName, value);
            return obj;
        } catch (IllegalAccessException | InvocationTargetException |
                NoSuchMethodException e) {
            log.error("Erro ao preencher a propriedade " + propertyName, e);
            return null;
        }
    }

    private static Object newInstance(Class clazz) {
        try {
            return ConstructorUtils.invokeConstructor(clazz, null);
        } catch (NoSuchMethodException | IllegalAccessException |
                InvocationTargetException | InstantiationException e) {
            log.error("Erro ao invocar contrutor default da classe " + clazz.getName(), e);
            return null;
        }
    }

    /**
     * Lê os dados da planilha, Se todas as colunas lidas estiverem vaziqas, retorna nulo.
     *
     * @param sheet
     * @param actualLine
     * @param actualCol
     * @param size
     * @return
     */
    private static List<String> readSheetValues(Sheet sheet, int actualLine, int actualCol, int size) {
        boolean dataFound = false;
        List<String> ret = Lists.newArrayList();
        Row row = sheet.getRow(actualLine);
        for (int i = actualCol; i < actualCol + size; i++) {
            String data = ExcelUtils.getCellAsString(row, i);
            ret.add(data);
            if (StringUtils.isNotBlank(data)) {
                dataFound = true;
            }
        }

        return dataFound ? ret : null;
    }

    private static void registerXMLGregorianCalendarConverter() {
        ConvertUtils.register(new Converter() {

            @Override
            public <T> T convert(Class<T> type, Object value) {
                if (String.class.isAssignableFrom(value.getClass())) {
                    String dtStr = (String) value;
                    try {
                        return (T) XMLGregorianCalendarConversionUtil.asXMLGregorianCalendar_Date(
                                dtStr, "dd-MM-yyyy");
                    } catch (ParseException e) {
                        try {
                            return (T) XMLGregorianCalendarConversionUtil.
                                    asXMLGregorianCalendar_Date(
                                            dtStr, "dd/MM/yyyy");
                        } catch (ParseException e1) {
                            String err = String.format(
                                    "A data %1s não está em um dos formatos %2s ou %3s", value,
                                    "dd-MM-yyyy", "dd/MM/yyyy");
                            log.error(err);
                            throw new RuntimeException(err);
                        }
                    }
                } else {
                    throw new RuntimeException(String.format("Argumento %1s é do tipo %2s quando "
                            + "deveria ser do tipo String", value, value.getClass()));
                }
            }
        }, XMLGregorianCalendar.class);
    }
}
