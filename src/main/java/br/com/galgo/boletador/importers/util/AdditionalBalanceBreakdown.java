/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.galgo.boletador.importers.util;

import iso.std.iso._20022.tech.xsd.semt_003_001.AdditionalBalanceInformation6;
import iso.std.iso._20022.tech.xsd.semt_003_001.FinancialInstrumentQuantity1Choice;
import iso.std.iso._20022.tech.xsd.semt_003_001.SecuritiesBalanceType7Code;
import iso.std.iso._20022.tech.xsd.semt_003_001.SubBalanceQuantity3Choice;
import iso.std.iso._20022.tech.xsd.semt_003_001.SubBalanceType6Choice;
import java.math.BigDecimal;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author joyce.oliveira
 */
public class AdditionalBalanceBreakdown {

    private static final Logger log = LoggerFactory.getLogger(AdditionalBalanceBreakdown.class);

    public void geraAdditionalBalanceBreakdown(AdditionalBalanceInformation6 infComplementar,
            String cd, String valor) {

        log.trace("cd={}; valor={}", cd, valor);

        SubBalanceType6Choice subBalTp = infComplementar.getSubBalTp();
        if (subBalTp == null) {
            subBalTp = new SubBalanceType6Choice();
            infComplementar.setSubBalTp(subBalTp);
        }
        if (cd != null) {
            SecuritiesBalanceType7Code codigo = SecuritiesBalanceType7Code.fromValue(cd);
            subBalTp.setCd(codigo);
        }

        SubBalanceQuantity3Choice qty = infComplementar.getQty();
        if (qty == null) {
            qty = new SubBalanceQuantity3Choice();
            infComplementar.setQty(qty);
        }

        FinancialInstrumentQuantity1Choice qtyII = qty.getQty();
        if (qtyII == null) {
            qtyII = new FinancialInstrumentQuantity1Choice();
            qty.setQty(qtyII);
        }

        if (valor != null) {
            BigDecimal valorFace = new BigDecimal(valor);
            qtyII.setFaceAmt(valorFace);
        }
    }
}
