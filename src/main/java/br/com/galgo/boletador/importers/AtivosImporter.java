/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.galgo.boletador.importers;

import br.anbimacom.balanceforsubaccountbrazil.BalForSubAcctBrData;
import br.com.galgo.boletador.importers.util.*;
import com.galgo.utils.ApplicationException;
import com.google.common.collect.Maps;
import com.sistemagalgo.schemaposicaoativos.BsnsMsgComplexType;
import com.sistemagalgo.schemaposicaoativos.GalgoAssBalStmtComplexType;
import iso.std.iso._20022.tech.xsd.semt_003_001.*;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.hssf.util.CellReference;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.datatype.DatatypeConfigurationException;
import java.text.ParseException;
import java.util.List;
import java.util.Map;

/**
 *
 * @author joyce.oliveira
 */
public class AtivosImporter extends SheetImporter {

    private static final Logger log = LoggerFactory.getLogger(AtivosImporter.class);
    private static final int HEADER_ROW_NUMBER = 2;
    private static final int DATA_ROW_NUMBER = HEADER_ROW_NUMBER + 1;
    private Map<String, Object> dataMap = Maps.newHashMap();
    AtivoSheetLineController ativoSheetLineController = null;

    public AtivosImporter(GalgoAssBalStmtComplexType doc, Sheet sheet) {
        super(doc, sheet);
    }

    @Override
    public void importSheet() {
        log.info("************** Importando Ativos utilizando a aba {}", sheet.getSheetName());
        Row row = sheet.getRow(DATA_ROW_NUMBER);
        if (row == null) {
            log.info("Não há ativos cadastrados já que getRow({}) retornou nulo.", DATA_ROW_NUMBER);
            log.info("Retornando");
            return;
        }

        List<BsnsMsgComplexType> listamensagem = glg.getBsnsMsg();
        log.debug(listamensagem == null ? "listamensagem  nula" : "listamensagem.size={}",
                listamensagem.size());

        for (BsnsMsgComplexType listaBsnsMsg : listamensagem) {

            Document document = listaBsnsMsg.getDocument();
            SecuritiesBalanceAccountingReportV04 sctiesBalAcctgRpt = document.getSctiesBalAcctgRpt();
            List<SubAccountIdentification16> subAcctDtls = sctiesBalAcctgRpt.getSubAcctDtls();

            //Enquanto houver informação na coluna 0, incluir informações de Custodiante
            //esta regra será alterada, o Custodiante só precisará
            //ser informado novamente se o ativo tiver um
            //Custodiante diferente, se o Custodiante for o mesmo, basta repetir
            //o bloco BalForSubAcct (Ana - ANBIMA deve atualizar informação no manual)
            //foi implementado conforme a regra atual, que deve informar um custodiante para
            //cada ativo
            for (int contAtivos = row.getRowNum(); ExcelUtils.getCellAsString(sheet, contAtivos, 0)
                    != null; contAtivos = ativoSheetLineController.getNextAtivoLine()) {

                log.debug("linha (row) = {}", contAtivos);
                ativoSheetLineController = new AtivoSheetLineController(contAtivos + 1);

                SubAccountIdentification16 ativo = new SubAccountIdentification16();
                subAcctDtls.add(ativo);

                //SET SfkpgAcct - Define as informações do custodiante
                int safekeepingAccountCol = 0;
                SecuritiesAccount14 sfkpgAcct = ativo.getSfkpgAcct();

                if (sfkpgAcct == null) {
                    sfkpgAcct = new SecuritiesAccount14();
                    ativo.setSfkpgAcct(sfkpgAcct);
                }

                //Gera bloco do Custodiante(SafekeepingAccount), deve passar no metodo
                //Id, Tipo do Id, Emissor, Nome do Custodiante
                int offset = 0;
                SafekeepingAccount.geraSafekeepingAccount(sfkpgAcct,
                        ExcelUtils.getCellAsString(sheet, contAtivos, safekeepingAccountCol
                                + offset++),
                        ExcelUtils.getCellAsString(sheet, contAtivos, safekeepingAccountCol
                                + offset++),
                        ExcelUtils.getCellAsString(sheet, contAtivos, safekeepingAccountCol
                                + offset++),
                        ExcelUtils.getCellAsString(sheet, contAtivos, safekeepingAccountCol
                                + offset++));

                //SET Indicador de Atividade - Coluna 4 (E)
                int activityIndCol = safekeepingAccountCol + 4;
                log.trace("Importando Indicador de Atividade da coluna {}", CellReference.
                        convertNumToColString(activityIndCol));
                if (ExcelUtils.getCellAsString(sheet, contAtivos, activityIndCol) != null) {
                    ativo.setActvtyInd(Boolean.parseBoolean(ExcelUtils.getCellAsString(sheet,
                            contAtivos, activityIndCol)));
                }

                //SET BalForSubAcct - Informaçoes do ativo
                List<AggregateBalanceInformation13> balForSubAcctList = ativo.getBalForSubAcct();
                AggregateBalanceInformation13 balForSubAcct = new AggregateBalanceInformation13();
                balForSubAcctList.add(balForSubAcct);

                //SET bloco de Identificações do Fundo - FinInstrmId
                SecurityIdentification14 finInstrmId = new SecurityIdentification14();
                balForSubAcct.setFinInstrmId(finInstrmId);

                //SET ISIN - se o fundo possuir
                final int finInstrmIdCol = activityIndCol + 1;
                String finInstrmIdStr = ExcelUtils.
                        getCellAsString(sheet, contAtivos, finInstrmIdCol);
                if (StringUtils.isNotBlank(finInstrmIdStr)) {
                    finInstrmId.setISIN(finInstrmIdStr.trim());
                }

                //SET FillFinInstrmId
                fillFinInstrmId(finInstrmId, contAtivos, finInstrmIdCol + 1);

                //Set bloco FinancialInstrumentAttributes
                int financialInstrumentAttributesCol = finInstrmIdCol + 5;
                FinancialInstrumentAttributes20 finInstrmAttrbts = balForSubAcct.
                        getFinInstrmAttrbts();
                if (finInstrmAttrbts == null) {
                    finInstrmAttrbts = new FinancialInstrumentAttributes20();
                    balForSubAcct.setFinInstrmAttrbts(finInstrmAttrbts);
                }

                FinancialInstrumentAttributes atributoAtivo = new FinancialInstrumentAttributes();
                //Informa os seguintes parametros:
                try {
                    offset = 0;
                    // 10 colunas
                    atributoAtivo.geraFinancialInstrumentAttributes(finInstrmAttrbts,
                            ExcelUtils.getCellAsString(sheet, contAtivos,
                                    financialInstrumentAttributesCol + offset++),
                            ExcelUtils.getCellAsString(sheet, contAtivos,
                                    financialInstrumentAttributesCol + offset++),
                            ExcelUtils.getCellAsString(sheet, contAtivos,
                                    financialInstrumentAttributesCol + offset++),
                            ExcelUtils.getCellAsString(sheet, contAtivos,
                                    financialInstrumentAttributesCol + offset++),
                            ExcelUtils.getCellAsString(sheet, contAtivos,
                                    financialInstrumentAttributesCol + offset++),
                            ExcelUtils.getCellAsString(sheet, contAtivos,
                                    financialInstrumentAttributesCol + offset++),
                            ExcelUtils.getCellAsString(sheet, contAtivos,
                                    financialInstrumentAttributesCol + offset++),
                            ExcelUtils.getCellAsString(sheet, contAtivos,
                                    financialInstrumentAttributesCol + offset++),
                            ExcelUtils.getCellAsString(sheet, contAtivos,
                                    financialInstrumentAttributesCol + offset++),
                            ExcelUtils.getCellAsString(sheet, contAtivos,
                                    financialInstrumentAttributesCol + offset++));
                } catch (ParseException | DatatypeConfigurationException ex) {
                    log.error("Erro ao recuperar FinancialInstrumentAttributes "
                            + "da planilha [linha " + contAtivos + "]", ex);
                }

                //Se tiver informação de Cupom nos Atributos do Fundo - Bloco CouponAttachedNumber
                int couponAttachedNumberCol = financialInstrumentAttributesCol + 10;
                if (ExcelUtils.getCellAsString(sheet, contAtivos, 19) != null) {
                    Number2Choice cpnAttchdNb = finInstrmAttrbts.getCpnAttchdNb();
                    if (cpnAttchdNb == null) {
                        cpnAttchdNb = new Number2Choice();
                        finInstrmAttrbts.setCpnAttchdNb(cpnAttchdNb);
                    }

                    offset = 0;
                    CpnAttchdNb cupom = new CpnAttchdNb();
                    cupom.gerarCpnAttchdNb(cpnAttchdNb,
                            ExcelUtils.getCellAsString(sheet, contAtivos, couponAttachedNumberCol
                                    + offset++),
                            ExcelUtils.getCellAsString(sheet, contAtivos, couponAttachedNumberCol
                                    + offset++),
                            ExcelUtils.getCellAsString(sheet, contAtivos, couponAttachedNumberCol
                                    + offset++),
                            ExcelUtils.getCellAsString(sheet, contAtivos, couponAttachedNumberCol
                                    + offset++));
                }

                //SET Preço de Emissão - Bloco SubscriptionPrice
                int subscriptionPriceCol = couponAttachedNumberCol + 4;
                if (ExcelUtils.getCellAsString(sheet, contAtivos, subscriptionPriceCol) != null) {
                    Price2 sbcptPric = finInstrmAttrbts.getSbcptPric();
                    if (sbcptPric == null) {
                        sbcptPric = new Price2();
                        finInstrmAttrbts.setSbcptPric(sbcptPric);
                    }

                    offset = 0;
                    SubscriptionPrice precoEmissao = new SubscriptionPrice();
                    precoEmissao.geraSubscriptionPrice(sbcptPric,
                            ExcelUtils.getCellAsString(sheet, contAtivos, subscriptionPriceCol
                                    + offset++),
                            ExcelUtils.getCellAsString(sheet, contAtivos, subscriptionPriceCol
                                    + offset++),
                            ExcelUtils.getCellAsString(sheet, contAtivos, subscriptionPriceCol
                                    + offset++));
                }

                //SET Preço de Vencimento - Bloco ConversionPrice
                int conversionPriceCol = subscriptionPriceCol + 3;
                if (ExcelUtils.getCellAsString(sheet, contAtivos, conversionPriceCol) != null) {
                    Price2 convsPric = finInstrmAttrbts.getConvsPric();
                    if (convsPric == null) {
                        convsPric = new Price2();
                        finInstrmAttrbts.setConvsPric(convsPric);
                    }
                    ConversionPrice precoVencimento = new ConversionPrice();
                    offset = 0;
                    precoVencimento.geraConversionPrice(convsPric,
                            ExcelUtils.getCellAsString(sheet, contAtivos, conversionPriceCol
                                    + offset++),
                            ExcelUtils.getCellAsString(sheet, contAtivos, conversionPriceCol
                                    + offset++),
                            ExcelUtils.getCellAsString(sheet, contAtivos, conversionPriceCol
                                    + offset++));
                }

                // SET ID Do Instrumento Financeiro Subjacente -
                // Bloco UnderlyingFinancialInstrumentIdentification
                // Colunas 29, 30,31
                //SET Saldo Total do Ativo - Bloco AggregateBalance
                int aggregateBalanceCol = conversionPriceCol + 6;
                if (ExcelUtils.getCellAsString(sheet, contAtivos, aggregateBalanceCol) != null) {
                    Balance1 aggtBal = balForSubAcct.getAggtBal();
                    if (aggtBal == null) {
                        aggtBal = new Balance1();
                        balForSubAcct.setAggtBal(aggtBal);
                    }

                    AggregateBalance saldo = new AggregateBalance();
                    saldo.geraAggregateBalance(aggtBal,
                            ExcelUtils.getCellAsString(sheet, contAtivos, aggregateBalanceCol),
                            ExcelUtils.getCellAsString(sheet, contAtivos, aggregateBalanceCol + 1));
                }

                //SET Detalhes do Preço - Bloco PriceDetailsFiller (permite repetição)
                List<PriceInformation5> pricDtlsLst = balForSubAcct.getPricDtls();

                int contPrice = contAtivos - 1;

                // Incluir PriceDetails enquanto os AggregateBalance for nulo
                /* Colunas 34 a 38 */
                int priceDetailsCol = aggregateBalanceCol + 2;
                do {
                    try {
                        contPrice++;
                        ativoSheetLineController.updateNextAtivoLine(contPrice + 1);
                        PriceInformation5 pricDtls = new PriceInformation5();
                        pricDtlsLst.add(pricDtls);
                        PriceDetailsFiller.fill(pricDtls, sheet, contPrice, priceDetailsCol);
                    } catch (ApplicationException e) {
                        log.error("Erro ao importar Bloco PriceDetails <PricDtls> na linha "
                                + contPrice, e);
                    }
                } while (!ExcelUtils.isBlank(sheet, contPrice + 1, priceDetailsCol)
                        && ExcelUtils.isBlank(sheet, contPrice + 1, aggregateBalanceCol));

                // SET Valor Financeiro Total - AccountBaseCurrencyAmounts <AcctBaseCcyAmts>
                // Colunas 39 a 47
                int accountBaseCurrencyAmountCol = priceDetailsCol + 5;
                BalanceAmounts1 acctBaseCcyAmts = new BalanceAmounts1();
                AccountBaseCurrencyAmountFiller.fill(acctBaseCcyAmts, sheet, contAtivos,
                        accountBaseCurrencyAmountCol);
                balForSubAcct.setAcctBaseCcyAmts(acctBaseCcyAmts);

                // SET Quantidade do ativos - BalanceBreakdown <BalBrkdwn>
                int balBrkDwnCol = accountBaseCurrencyAmountCol + 9;
                fillBalanceBreakdown(contAtivos, balBrkDwnCol, balForSubAcct,
                        accountBaseCurrencyAmountCol);

                // SET Divisão do Saldo Adicional - AdditionalBalanceBreakdown
                // <AddtlBalBrkdwn> [0..*]
                int addtlBalBrkdwnCol = balBrkDwnCol + 5;
                fillAdditionalBalanceBreakdown(contAtivos, addtlBalBrkdwnCol, balForSubAcct, 0);

                /**
                 * ******************************************************************************
                 * Supplementary Data
                 * ******************************************************************************
                 */
                // Risk Level
                int riskLevelCol = addtlBalBrkdwnCol + 4;
                SupplementaryData supplData = new SupplementaryData(sheet, contAtivos, riskLevelCol);
                BalForSubAcctBrData balForSubAcctBrData = supplData.getBalForSubAcctBrData();
//                BalForSubAcctBrData balForSubAcctBrData = new SupplementaryData.getBalForSubAcctBrData(
//                        balForSubAcct);
                if (balForSubAcctBrData != null) {
                    fillSupplementaryData(balForSubAcct, balForSubAcctBrData);
                }
//                JaxbBeanFiller.fill(sheet, contAtivos, riskLevelCol,
//                        BalForSubAcctBrData.RiskLv.class, "cd", balForSubAcctBrData, "riskLv");
//                if (riskLv != null) {
//                    balForSubAcctBrData.setRiskLv(riskLv);
//                }

                // HedgeType (Tipo de Hedge) / Code
//                int hedgTpCol = riskLevelCol + 1;
//                BalForSubAcctBrData.HedgTp hedgTp = HedgTpFiller.fill(sheet, contAtivos, hedgTpCol);
//                if (hedgTp != null) {
//                    balForSubAcctBrData.setHedgTp(hedgTp);
//                }

            }// != null; contAtivos = ativoSheetLineController.getNextAtivoLine())

        } // Fim de for (BsnsMsgComplexType listaBsnsMsg : listamensagem) {

    }

    private void fillSupplementaryData(AggregateBalanceInformation13 balForSubAcct,
            BalForSubAcctBrData balForSubAcctBrData) {
        List<SupplementaryData1> splmtryDataList = balForSubAcct.getSplmtryData();

        SupplementaryData1 splmtryData = new iso.std.iso._20022.tech.xsd.semt_003_001.ObjectFactory().
                createSupplementaryData1();
        splmtryDataList.add(splmtryData);

        SupplementaryDataEnvelope1 envlp = new SupplementaryDataEnvelope1();
        splmtryData.setEnvlp(envlp);

        envlp.setBalForSubAcctBrData(balForSubAcctBrData);
    }

    /**
     * SET Divisão do Saldo Adicional - AdditionalBalanceBreakdown &lt;AddtlBalBrkdwn&gt; [0..*]
     *
     * @param contAtivos Linha atual do ativo
     * @param addtlBalBrkdwnCol Coluna inicial de importação
     * @param balForSubAcct objeto pai
     * @param referenceCol Coluna utilizada para se determinar se a próxima linha é uma repetição ou
     * não
     */
    private void fillAdditionalBalanceBreakdown(int contAtivos, int addtlBalBrkdwnCol,
            AggregateBalanceInformation13 balForSubAcct, int referenceCol) {
        log.debug("SET Divisão do Saldo Adicional - AdditionalBalanceBreakdown "
                + "<AddtlBalBrkdwn> [0..*]");
        int contAddtlBalBrkdwn = contAtivos - 1;
        List<AdditionalBalanceInformation6> addtlBalBrkdwnList
                = balForSubAcct.getAddtlBalBrkdwn();
        do {
            contAddtlBalBrkdwn++;
            ativoSheetLineController.updateNextAtivoLine(contAddtlBalBrkdwn + 1);
            AdditionalBalanceInformation6 addtlBalBrkdwn = AdditionalBalanceInformation6Importer.
                    build(sheet, contAddtlBalBrkdwn, addtlBalBrkdwnCol);
            balForSubAcct.getAddtlBalBrkdwn().add(addtlBalBrkdwn);
        } while ((!ExcelUtils.isBlank(sheet, contAddtlBalBrkdwn + 1, addtlBalBrkdwnCol)
                && ExcelUtils.
                isBlank(sheet, contAddtlBalBrkdwn + 1, referenceCol)));

    }

    /**
     * SET Quantidade do ativos - BalanceBreakdown &lt;BalBrkdwn&gt; usando a coluna 48 como coluna
     * inicial
     *
     * @param contAtivos Linha inicial na planilha com os dados a serem importados
     * @param balBrkDwnCol Coluna inicial na planilha com os dados a serem importados
     * @param balForSubAcct
     * @param accountBaseCurrencyAmountCol Coluna utilizada para se determinar se a próxima linha é
     * uma repetição ou não
     */
    private void fillBalanceBreakdown(int contAtivos, int balBrkDwnCol,
            AggregateBalanceInformation13 balForSubAcct, int accountBaseCurrencyAmountCol) {
        //
        log.debug("SET Quantidade do ativo - BalanceBreakdown <BalBrkdwn>");
        int contBalBrkDwn = contAtivos - 1;
        List<SubBalanceInformation6> balBrkdwnList = balForSubAcct.getBalBrkdwn();
        do {
            try {
                contBalBrkDwn++;
                ativoSheetLineController.updateNextAtivoLine(contBalBrkDwn + 1);
                balBrkdwnList.add(SubBalanceInformation6Importer.build(sheet, contBalBrkDwn,
                        balBrkDwnCol));
            } catch (ApplicationException e) {
                log.error("Erro ao importar Bloco BalanceBreakdown <BalBrkdwn> na linha "
                        + contBalBrkDwn, e);
            }
        } while (!ExcelUtils.isBlank(sheet, contBalBrkDwn + 1, balBrkDwnCol)
                && ExcelUtils.
                isBlank(sheet, contBalBrkDwn + 1, accountBaseCurrencyAmountCol));
    }

    /**
     * Preenche a seção FillFinInstrmId
     *
     * @param finInstrmId
     * @param row
     * @param firstCol
     */
    private void fillFinInstrmId(SecurityIdentification14 finInstrmId, int row, int firstCol) {
        List<OtherIdentification1> othrIdLst = finInstrmId.getOthrId();

        //Enquanto a coluna firstCol tiver conteudo e a coluna zero for nula,
        //deve incluir um novo Identificador, a primeira vez não deve
        //verificar se a primeira coluna está nula
        int currentLine = row - 1;
        do {
            currentLine++;
            ativoSheetLineController.updateNextAtivoLine(currentLine + 1);
            if (!ExcelUtils.isBlank(sheet, currentLine, firstCol)) {
                OtherIdentification1 othrId = new OtherIdentification1();
                othrIdLst.add(othrId);

                OtherIdentification id = new OtherIdentification();
                String suffix = ExcelUtils.getCellAsString(sheet, currentLine, firstCol + 1);
                if (StringUtils.isNotBlank(suffix)) {
                    id.setSuffix(suffix);
                }

                if (ExcelUtils.getCellAsString(sheet, currentLine, firstCol + 2) != null) {
                    id.geraIdentificacaoCode(othrId,
                            ExcelUtils.getCellAsString(sheet, currentLine, firstCol),
                            ExcelUtils.getCellAsString(sheet, currentLine, firstCol + 2));
                } else {
                    id.geraIdentificacaoProprietary(othrId,
                            ExcelUtils.getCellAsString(sheet, currentLine, firstCol),
                            ExcelUtils.getCellAsString(sheet, currentLine, firstCol + 3));
                }
            }
        } while (null == ExcelUtils.getCellAsString(sheet, currentLine + 1, 0) && !ExcelUtils.
                isBlank(sheet, currentLine + 1, firstCol));
        //Fim do bloco de Identificações do Fundo - FinInstrmId
    }

}
