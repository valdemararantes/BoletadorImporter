/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.galgo.boletador.importers.util;

import br.com.galgo.boletador.importers.ExcelUtils;
import java.math.BigDecimal;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.ss.usermodel.Sheet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Classe que importa o valor, moeda e sinal em 3 células vizinhas, nesta ordem
 * @author valdemar.arantes
 */
public class ValorMoedaSinalImporter {

    private static final Logger log = LoggerFactory.getLogger(ValorMoedaSinalImporter.class);

    private BigDecimal valor;
    private String moeda;
    private Boolean sinal;

    public ValorMoedaSinalImporter(Sheet sheet, int rowNr, int colNr) {
        // Valor
        String strValue = ExcelUtils.getCellAsString(sheet, rowNr, colNr);

        // Se não há valor, não há nada a ser importado nesta e nas próximas duas células
        if (StringUtils.isBlank(strValue)) {
            return;
        }

        try {
            valor = new BigDecimal(strValue);
        } catch (Exception e) {
            log.error("Erro ao importar valor da célula [" + rowNr + "," + colNr + "]="
                    + strValue, e);
        }

        // Moeda
        moeda = ExcelUtils.getCellAsString(sheet, rowNr, colNr + 1);

        // Sinal
        String strSinal = ExcelUtils.getCellAsString(sheet, rowNr, colNr + 2);
        sinal = !"false".equalsIgnoreCase(strSinal);
    }

    public BigDecimal getValor() {
        return valor;
    }

    public String getMoeda() {
        return moeda;
    }

    public Boolean isSinal() {
        return sinal;
    }

}
