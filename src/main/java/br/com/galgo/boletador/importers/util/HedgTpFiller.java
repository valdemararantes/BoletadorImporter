/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.galgo.boletador.importers.util;

import br.anbimacom.balanceforsubaccountbrazil.BalForSubAcctBrData;
import br.com.galgo.boletador.importers.ExcelUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.ss.usermodel.Sheet;

/**
 *
 * @author valdemar.arantes
 */
public class HedgTpFiller {

    public static BalForSubAcctBrData.HedgTp fill(Sheet sheet, int contAtivos, int hedgTpCol) {
        String hedge = ExcelUtils.getCellAsString(sheet, contAtivos, hedgTpCol);
        if (StringUtils.isBlank(hedge)) {
            return null;
        } else {
            BalForSubAcctBrData.HedgTp hedgTp = new BalForSubAcctBrData.HedgTp();
            hedgTp.setCd(hedge);
            return hedgTp;
        }
    }

}
