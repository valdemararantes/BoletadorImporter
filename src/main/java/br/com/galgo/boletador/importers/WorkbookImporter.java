/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.galgo.boletador.importers;

import com.sistemagalgo.schemaposicaoativos.GalgoAssBalStmtComplexType;
import java.io.InputStream;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.OfficeXmlFileException;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author valdemar.arantes
 */
public class WorkbookImporter {

    private static final Logger log = LoggerFactory.getLogger(WorkbookImporter.class);
    private InputStream workbookInputStream;

    public WorkbookImporter(InputStream workbookInputStream) {
        this.workbookInputStream = workbookInputStream;
    }

    public GalgoAssBalStmtComplexType importWorkbook() {
        InputStream inp = null;
        try {
            log.debug("Lendo a planilha");

            Workbook wb;
            try {
                wb = new HSSFWorkbook(workbookInputStream);
            } catch (OfficeXmlFileException e) {
                wb = new XSSFWorkbook(workbookInputStream);
            }
            return new SheetsImporter(wb).importSheets();
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        } finally {
            if (inp != null) {
                try {
                    inp.close();
                } catch (Exception e) {
                }
            }
        }
    }
}
