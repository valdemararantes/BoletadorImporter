/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.galgo.boletador.importers.util;

import iso.std.iso._20022.tech.xsd.semt_003_001.IdentificationSource3Choice;
import iso.std.iso._20022.tech.xsd.semt_003_001.OtherIdentification1;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author joyce.oliveira
 */
public class OtherIdentification {

    private static final Logger log = LoggerFactory.getLogger(OtherIdentification.class);
    private String suffix;

    /**
     * @return Identificação do emissor - &lt;Sfx&gt;
     */
    public String getSuffix() {
        return suffix;
    }

    /**
     *
     * @param suffix Identificação do emissor - &lt;Sfx&gt;
     */
    public void setSuffix(String suffix) {
        this.suffix = suffix;
    }

    public void geraIdentificacaoProprietary(OtherIdentification1 identificador,
            String id, String tpIdentificador) {
        log.trace("identificador={}, id={}, tpIdentificador={}", identificador, id, tpIdentificador);
        identificador.setId(id);

        IdentificationSource3Choice tp = identificador.getTp();
        if (tp == null) {
            tp = new IdentificationSource3Choice();
            identificador.setTp(tp);
        }
        tp.setPrtry(tpIdentificador);

        fillSuffix(identificador);
    }

    public void geraIdentificacaoCode(OtherIdentification1 identificador, String id,
            String cdIdentificador) {
        log.trace("identificador={}, id={}, cdIdentificador={}", identificador, id, cdIdentificador);
        identificador.setId(id);

        IdentificationSource3Choice tp = identificador.getTp();
        if (tp == null) {
            tp = new IdentificationSource3Choice();
            identificador.setTp(tp);
        }
        tp.setCd(cdIdentificador);

        fillSuffix(identificador);
    }

    private void fillSuffix(OtherIdentification1 identificador) {
        if (StringUtils.isNotBlank(suffix)) {
            identificador.setSfx(suffix);
        }
    }
}
