/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.galgo.boletador.importers.util;

import iso.std.iso._20022.tech.xsd.semt_003_001.GenericIdentification20;
import iso.std.iso._20022.tech.xsd.semt_003_001.PurposeCode2Choice;
import iso.std.iso._20022.tech.xsd.semt_003_001.SecuritiesAccount14;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author joyce.oliveira
 */
public class SafekeepingAccount {

    private static final Logger log = LoggerFactory.getLogger(SafekeepingAccount.class);

    /**
     *
     * @param custodiante -Objeto do tipo SecuritiesAccount14 que terá os dados preenchidos
     * @param id - Numero do CNPJ ou outro identificador
     * @param tipoId - Tipo do Identificador, por exemplo CNPJ
     * @param emissor - Emissor do Identificador
     * @param nomeCustodiante - Nome do Custodiante
     */
    public static void geraSafekeepingAccount(SecuritiesAccount14 custodiante, String id,
            String tipoId, String emissor, String nomeCustodiante) {

        log.trace("id={}, tipoId={}, ...", id, tipoId);

        if (id != null) {
            custodiante.setId(id);
        }

        if (nomeCustodiante != null) {
            custodiante.setNm(nomeCustodiante);
        }

        PurposeCode2Choice tp = custodiante.getTp();
        if (tp == null) {
            tp = new PurposeCode2Choice();
            custodiante.setTp(tp);
        }

        GenericIdentification20 prtry = tp.getPrtry();
        if (prtry == null) {
            prtry = new GenericIdentification20();
            tp.setPrtry(prtry);
        }

        if (tipoId != null) {
            prtry.setId(tipoId);
        }

        if (emissor != null) {
            prtry.setIssr(emissor);
        }
    }
}
