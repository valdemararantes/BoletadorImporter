/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.galgo.boletador.importers.util;

import br.anbimacom.balanceforsubaccountbrazil.BalForSubAcctBrData;
import static br.com.galgo.boletador.importers.util.RealStatePrtflSupplData.SIZE;
import org.apache.poi.ss.usermodel.Sheet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author valdemar.arantes
 */
class ValuationInformationSuppl extends TagBuilder<BalForSubAcctBrData.RealStatePrtfl.ValtInf> {

    private static final Logger log = LoggerFactory.getLogger(ValuationInformationSuppl.class);
    public static final int SIZE = 7;

    public ValuationInformationSuppl(Sheet sheet, int sheetLine, int sheetCol) {
        super(sheet, sheetLine, sheetCol, SIZE);
    }

    BalForSubAcctBrData.RealStatePrtfl.ValtInf getElement() {
        if (element == null) {
            return null;
        }

        populate();
        return element;
    }

    private void populate() {
        // Total Valuation Amount <TtlValAmt> [0..1]
        int actualCol = sheetCol;
        JaxbBeanFiller.fill(sheet, sheetLine, actualCol, null, element, "ttlValAmt");

        // Valuation date <ValDate> [0..1]
        actualCol++;
        JaxbBeanFiller.fill(sheet, sheetLine, actualCol, null, element, "valDate");

        // Type of Evaluator / Code <EvaTp> / <Cd> [0..1]
        actualCol++;
        JaxbBeanFiller.fill(sheet, sheetLine, actualCol, "cd", element, "evaTp");

        // Identification <Id> [0..1]
        actualCol++;
        JaxbBeanFiller.fill(sheet, sheetLine, actualCol, null, element, "id");

        // Proprietary
        actualCol++;
        element.setPrtry(new PrtryValtInfSupplData(sheet, sheetLine, actualCol).getElement());
    }

}
