/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.galgo.boletador.importers.util;

import iso.std.iso._20022.tech.xsd.semt_003_001.ActiveOrHistoricCurrencyAnd13DecimalAmount;
import iso.std.iso._20022.tech.xsd.semt_003_001.Price2;
import iso.std.iso._20022.tech.xsd.semt_003_001.PriceRateOrAmountChoice;
import iso.std.iso._20022.tech.xsd.semt_003_001.PriceValueType1Code;
import iso.std.iso._20022.tech.xsd.semt_003_001.YieldedOrValueType1Choice;
import java.math.BigDecimal;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author joyce.oliveira
 */
public class ConversionPrice {

    private static final Logger log = LoggerFactory.getLogger(ConversionPrice.class);

    public void geraConversionPrice(Price2 precoVencimento, String tipoPreco, String moeda,
            String valor) {

        log.trace("precoVencimento={}, tipoPreco={}, moeda={}, valor={}", new Object[]{
            precoVencimento, tipoPreco, moeda, valor});

        YieldedOrValueType1Choice tp = precoVencimento.getTp();
        if (tp == null) {
            tp = new YieldedOrValueType1Choice();
            precoVencimento.setTp(tp);
        }

        PriceValueType1Code tpPrc = PriceValueType1Code.fromValue(tipoPreco);
        tp.setValTp(tpPrc);

        PriceRateOrAmountChoice val = precoVencimento.getVal();
        if (val == null) {
            val = new PriceRateOrAmountChoice();
            precoVencimento.setVal(val);
        }

        ActiveOrHistoricCurrencyAnd13DecimalAmount amt = val.getAmt();
        if (amt == null) {
            amt = new ActiveOrHistoricCurrencyAnd13DecimalAmount();
            val.setAmt(amt);
        }

        amt.setCcy(moeda);

        BigDecimal vlr = new BigDecimal(valor);
        amt.setValue(vlr);
    }
}
