/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.galgo.boletador.importers.util;

import iso.std.iso._20022.tech.xsd.semt_003_001.Balance1;
import iso.std.iso._20022.tech.xsd.semt_003_001.BalanceQuantity4Choice;
import iso.std.iso._20022.tech.xsd.semt_003_001.FinancialInstrumentQuantity1Choice;
import iso.std.iso._20022.tech.xsd.semt_003_001.Quantity6Choice;
import iso.std.iso._20022.tech.xsd.semt_003_001.ShortLong1Code;
import java.math.BigDecimal;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author joyce.oliveira
 */
public class AggregateBalance {

    private static final Logger log = LoggerFactory.getLogger(AggregateBalance.class);

    /**
     *
     * @param saldoTotal
     * @param indShortLogn
     * @param qntdAtivo
     */
    public void geraAggregateBalance(Balance1 saldoTotal, String indShortLogn, String qntdAtivo) {

        log.trace("saldoTotal={}, indShortLogn={}, qntdAtivo={}", new Object[]{saldoTotal, indShortLogn, qntdAtivo});

        if (indShortLogn != null) {
            ShortLong1Code indicador = ShortLong1Code.fromValue(indShortLogn);
            saldoTotal.setShrtLngInd(indicador);
        }

        BalanceQuantity4Choice qty = saldoTotal.getQty();
        if (qty == null) {
            qty = new BalanceQuantity4Choice();
            saldoTotal.setQty(qty);
        }

        Quantity6Choice qtyII = qty.getQty();
        if (qtyII == null) {
            qtyII = new Quantity6Choice();
            qty.setQty(qtyII);
        }

        FinancialInstrumentQuantity1Choice qtyIII = qtyII.getQty();
        if (qtyIII == null) {
            qtyIII = new FinancialInstrumentQuantity1Choice();
            qtyII.setQty(qtyIII);
        }

        if (qntdAtivo != null) {
            BigDecimal quantidadeAtivos = new BigDecimal(qntdAtivo);
            qtyIII.setUnit(quantidadeAtivos);
        }

    }
}
