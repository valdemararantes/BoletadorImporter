/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.galgo.boletador.importers;

import com.galgo.utils.ApplicationException;

/**
 *
 * @author valdemar.arantes
 */
public class NonExistentRowException extends ApplicationException {

    public NonExistentRowException() {
    }

    public NonExistentRowException(String message) {
        super(message);
    }

    public NonExistentRowException(Throwable cause) {
        super(cause);
    }

    public NonExistentRowException(String message, Throwable cause) {
        super(message, cause);
    }

}
