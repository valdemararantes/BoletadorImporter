/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.galgo.boletador.importers.util;

import br.anbimacom.balanceforsubaccountbrazil.BalForSubAcctBrData;
import br.com.galgo.boletador.importers.ExcelUtils;
import br.com.galgo.boletador.importers.util.TreeBuilder.Node;
import com.google.common.collect.Lists;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Classe que cria e popula um objeto BalForSubAcctBrData a partir de uma planilha Excel.
 *
 * @author joyce.oliveira
 */
public class SupplementaryData {

    private static final Logger log = LoggerFactory.getLogger(SupplementaryData.class);

    // Quantidade de coluna na planilha utilizadas para Supplementary Data
    public final static int SIZE = 45;

    private BalForSubAcctBrData balForSubAcctBrData;
    private final Sheet sheet;
    private final int sheetLine;
    private final int sheetCol;

    public SupplementaryData(Sheet sheet, int sheetLine, int sheetCol) {
        this.sheet = sheet;
        this.sheetLine = sheetLine;
        this.sheetCol = sheetCol;

        if (existSupplDataInSheet()) {
            balForSubAcctBrData = new BalForSubAcctBrData();
        }
    }

    /**
     * Cria uma nova instância de BalForSubAcctBrData e a inclui na tag SplmtryData se houver dados
     * na planlha na linha passada no contrutor e nas colunas relativas ao SupplementaryData. Se não
     * houver esses dados, retorna null.
     *
     * @return Instância de BalForSubAcctBrData populada com os dados da planilha
     */
    public BalForSubAcctBrData getBalForSubAcctBrData() {
        // Não há dados na linha e nas colunas da planilha relativas ao SupplementaryData
        if (balForSubAcctBrData == null) {
            return null;
        }

        populate();

        return balForSubAcctBrData;
    }

    /**
     * Verifica se existem dados nas colunas da planilha relativas ao SupplementaryData
     *
     * @return false -> não há dados; true -> há dados
     */
    private boolean existSupplDataInSheet() {
        Row row = sheet.getRow(sheetLine);
        for (int col = sheetCol; col < sheetCol + SIZE; col++) {
            Cell cell = row.getCell(col);
            if (cell != null && (cell.getCellType() != Cell.CELL_TYPE_BLANK)) {
                return true;
            }
        }
        log.info("Nã foram encontrado dados na planilha para SupplementaryData na linhe {}",
                sheetLine);
        return false;
    }

    /**
     * Popula o objeto balForSubAcctBrData com os dados da planilha
     */
    private void populate() {

        TreeBuilder treeBuilder = new TreeBuilder(BalForSubAcctBrData.class, "balForSubAcctBrData");
        Tree<TreeBuilder.Node> tree = treeBuilder.buildTree();

        List<Node> leaves = tree.getLeaves(tree.getHead());
        log.debug("{} folhas encontradas na árvore", leaves.size());

        // Logando todas as propriedades aninhadas das folhas da árvores de classes
        for (Node leaf : leaves) {
            log.debug("NestedPropertyName de {} | {}", leaf, treeBuilder.getNestePropertyName(leaf));
        }

        Map<String, Object> bean = new HashMap<>();
        bean.put("balForSubAcctBrData", balForSubAcctBrData);

        int qtdCols = leaves.size();
        int firstCol = sheetCol;
        Row row = sheet.getRow(sheetLine);
        for (int i = firstCol; i < firstCol + qtdCols; i++) {
            String data = ExcelUtils.getCellAsString(row, i);
            if (StringUtils.isBlank(data)) {
                continue;
            }
            String propName = treeBuilder.getNestePropertyName(leaves.get(i - firstCol));
            try {
                com.galgo.utils.BeanUtils.setProperty(bean, propName, data);
            } catch (Exception e) {
                log.error("Erro ao atribuir {} à propriedade {} do objeto {}", data, propName, bean);
            }
        }

        log.info("FIM **************************");

//        // Risk Level
//        int actualCol = sheetCol;
//        JaxbBeanFiller.fill(sheet, sheetLine, actualCol, "cd", balForSubAcctBrData, "riskLv");
//
//        // HedgeType (Tipo de Hedge) / Code
//        actualCol++;
//        JaxbBeanFiller.fill(sheet, sheetLine, actualCol, "cd", balForSubAcctBrData, "hedgTp");
//
//        // Debentures <Debe> [0..1]
//        actualCol++;
//        JaxbBeanFiller.fill(sheet, sheetLine, actualCol,
//                Lists.newArrayList("convInd", "profSharInd"), balForSubAcctBrData, "debe", false, -1);
//
//        // UnderlyingAssetMaturityDate <UndrlygSctiesMtrtyDt> [0..1]
//        actualCol += 3;
//        JaxbBeanFiller.fill(sheet, sheetLine, actualCol, null, balForSubAcctBrData,
//                "undrlygSctiesMtrtyDt");
//
//        // Swaps <Swap> [0..1]
//        actualCol++;
//        balForSubAcctBrData.setSwap(new SwapSupplData(sheet, sheetLine, actualCol).getSwap());
//
//        // Real Estate Portfolio (Carteita Imobiliária) <RealStatePrtfl>[0..1]
//        actualCol += SwapSupplData.SIZE;
//        balForSubAcctBrData.setRealStatePrtfl(new RealStatePrtflSupplData(sheet, sheetLine,
//                actualCol).getRealStatePrtfl());
//
    }
}
