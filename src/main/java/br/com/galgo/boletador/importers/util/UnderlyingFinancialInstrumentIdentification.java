/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.galgo.boletador.importers.util;

import iso.std.iso._20022.tech.xsd.semt_003_001.OtherIdentification1;
import iso.std.iso._20022.tech.xsd.semt_003_001.SecurityIdentification14;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author joyce.oliveira
 */
public class UnderlyingFinancialInstrumentIdentification {

    private static final Logger log = LoggerFactory.getLogger(UnderlyingFinancialInstrumentIdentification.class);

    public void geraUnderlyingFinancialInstrumentIdentification(
            SecurityIdentification14 idInstrumento, String isin,
            String id, String cod) {

        log.trace("isin={}, id={}, ...", isin, id);

        if (isin != null) {
            idInstrumento.setISIN(isin);
        }


        if(id != null){
                    List<OtherIdentification1> othrIdLst = idInstrumento.getOthrId();
                    OtherIdentification1 othrId = new OtherIdentification1();

                    othrIdLst.add(othrId);
        }

    }
}
