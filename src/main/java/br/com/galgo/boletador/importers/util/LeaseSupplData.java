/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.galgo.boletador.importers.util;

import br.anbimacom.balanceforsubaccountbrazil.BalForSubAcctBrData;
import org.apache.poi.ss.usermodel.Sheet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author valdemar.arantes
 */
class LeaseSupplData extends TagBuilder<BalForSubAcctBrData.RealStatePrtfl.Lease> {

    private static final Logger log = LoggerFactory.getLogger(LeaseSupplData.class);
    public static final int SIZE = 3;

    LeaseSupplData(Sheet sheet, int sheetLine, int sheetCol) {
        super(sheet, sheetLine, sheetCol, SIZE);
    }

    public BalForSubAcctBrData.RealStatePrtfl.Lease getElement() {
        if (element == null) {
            return null;
        }

        populate();
        return element;
    }

    private void populate() {
        // Type / Code
        int actualCol = sheetCol;
        JaxbBeanFiller.fill(sheet, sheetLine, actualCol, "cd", element, "tp");
//        element.getAmt().setValue(BigDecimal.ZERO);
//        element.getAmt().setCcy("");

        // Amountt / value
        actualCol++;
        JaxbBeanFiller.fill(sheet, sheetLine, actualCol, "value", element, "amt");
        

        // Amountt / Ccy
        actualCol++;
        JaxbBeanFiller.fill(sheet, sheetLine, actualCol, "ccy", element, "amt");
    }
}
