/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.galgo.boletador.importers.util;

import iso.std.iso._20022.tech.xsd.semt_003_001.FinancialInstrumentQuantity1Choice;
import iso.std.iso._20022.tech.xsd.semt_003_001.SecuritiesBalanceType12Code;
import iso.std.iso._20022.tech.xsd.semt_003_001.SubBalanceInformation6;
import iso.std.iso._20022.tech.xsd.semt_003_001.SubBalanceQuantity3Choice;
import iso.std.iso._20022.tech.xsd.semt_003_001.SubBalanceType5Choice;
import java.math.BigDecimal;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author joyce.oliveira
 */
public class BalanceBreakdown {

    private static final Logger log = LoggerFactory.getLogger(BalanceBreakdown.class);

    public void geraBalanceBreakdown(SubBalanceInformation6 qntdAtivo, String cod, String quantidade,
            String detalhesAdd) {

        log.trace("qntdAtivo={}, cod={}, quantidade={}, detalhesAdd={}",
                new Object[]{qntdAtivo, cod, quantidade, detalhesAdd});

        SubBalanceType5Choice subBalTp = qntdAtivo.getSubBalTp();
        if (subBalTp == null) {
            subBalTp = new SubBalanceType5Choice();
            qntdAtivo.setSubBalTp(subBalTp);
        }

        if (cod != null) {
            SecuritiesBalanceType12Code codigo = SecuritiesBalanceType12Code.fromValue(cod);
            subBalTp.setCd(codigo);
        }

        SubBalanceQuantity3Choice qty = qntdAtivo.getQty();
        if (qty == null) {
            qty = new SubBalanceQuantity3Choice();
            qntdAtivo.setQty(qty);
        }

        FinancialInstrumentQuantity1Choice qtyII = qty.getQty();
        if (qtyII == null) {
            qtyII = new FinancialInstrumentQuantity1Choice();
            qty.setQty(qtyII);
        }

        if (quantidade != null) {
            BigDecimal qntd = new BigDecimal(quantidade);
            qtyII.setUnit(qntd);
        }

        if (detalhesAdd != null) {
            qntdAtivo.setSubBalAddtlDtls(detalhesAdd);
        }
    }
}
