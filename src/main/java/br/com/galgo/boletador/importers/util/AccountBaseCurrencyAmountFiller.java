/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.galgo.boletador.importers.util;

import br.com.galgo.boletador.importers.ExcelUtils;
import iso.std.iso._20022.tech.xsd.semt_003_001.ActiveOrHistoricCurrencyAndAmount;
import iso.std.iso._20022.tech.xsd.semt_003_001.AmountAndDirection6;
import iso.std.iso._20022.tech.xsd.semt_003_001.BalanceAmounts1;
import java.math.BigDecimal;
import org.apache.poi.ss.usermodel.Sheet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Classe que preenche a instância de AccountBaseCurrencyAmount com valores da planilha
 *
 * @author valdemar.arantes
 */
public class AccountBaseCurrencyAmountFiller {

    private static final Logger log = LoggerFactory.getLogger(AccountBaseCurrencyAmountFiller.class);

    /**
     * Preenche os dados de accountBaseCurrencyAmount utilizando 7 colunas: initialCol a initialCol
     * + 6. Os dados nas colunas devem ser:
     * <ul>
     * <li>Holding Value - Valor do Saldo
     * <li>Holding Value - Moeda
     * <li>Holding Value - Sinal (Seta o false se o valor for "false" e true caso contrário
     * <li>Book Value - Valor
     * <li>Book Value - Moeda
     * <li>Book Value - Sinal
     * <li>Ganhos e Perdas não Realizados - Valor
     * <li>Ganhos e Perdas não Realizados - Moeda
     * <li>Ganhos e Perdas não Realizados - Sinal
     * </ul>
     *
     * @param accountBaseCurrencyAmount Instância que será preenchida
     * @param sheet Planilha com os dados a serem utilizados no preenchimento
     * @param rowNr Linha da planilha com os dados
     * @param initialCol Coluna inicial com os dados
     */
    public static void fill(BalanceAmounts1 accountBaseCurrencyAmount, Sheet sheet, final int rowNr,
            int initialCol) {

        log.debug("Iniciando o preenchimento de  AccountBaseCurrencyAmounts <AcctBaseCcyAmts> "
                + "com a rowNr={} e initialCol = {}", rowNr, initialCol);

        // SET Holding Value
        AmountAndDirection6 holdingValue = buildAmountAndDirection6(sheet, rowNr, initialCol);
        accountBaseCurrencyAmount.setHldgVal(holdingValue);

        // SET Book Value
        AmountAndDirection6 bookVal = buildAmountAndDirection6(sheet, rowNr, initialCol + 3);
        accountBaseCurrencyAmount.setBookVal(bookVal);

        // SET Ganhos e Perdas não Realizados
        AmountAndDirection6 urlsdGnLoss = buildAmountAndDirection6(sheet, rowNr, initialCol + 6);
        accountBaseCurrencyAmount.setUrlsdGnLoss(urlsdGnLoss);
    }

    private static AmountAndDirection6 buildAmountAndDirection6(Sheet sheet, int rowNr, int colNr) {
        AmountAndDirection6 retValue = null;
        ActiveOrHistoricCurrencyAndAmount amount = new ActiveOrHistoricCurrencyAndAmount();
        ValorMoedaSinalImporter vmsImporter = new ValorMoedaSinalImporter(sheet, rowNr, colNr);
        if (vmsImporter.getValor() != null) {
            amount.setValue(vmsImporter.getValor());
            amount.setCcy(vmsImporter.getMoeda());
            retValue = new AmountAndDirection6();
            retValue.setAmt(amount);
            retValue.setSgn(vmsImporter.isSinal());
        }
        return retValue;
    }
}
