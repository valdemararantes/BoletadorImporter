/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.galgo.boletador.importers.util;

import br.anbimacom.balanceforsubaccountbrazil.BalForSubAcctBrData;
import org.apache.poi.ss.usermodel.Sheet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author valdemar.arantes
 */
class PrtryValtInfSupplData extends TagBuilder<BalForSubAcctBrData.RealStatePrtfl.ValtInf.Prtry> {

    private static final Logger log = LoggerFactory.getLogger(PrtryValtInfSupplData.class);
    public static final int SIZE = 3;

    PrtryValtInfSupplData(Sheet sheet, int sheetLine, int sheetCol) {
        super(sheet, sheetLine, sheetCol, SIZE);
    }

    public BalForSubAcctBrData.RealStatePrtfl.ValtInf.Prtry getElement() {
        if (element == null) {
            return null;
        }

        populate();
        return element;
    }

    private void populate() {
        // Id
        int actualCol = sheetCol;
        JaxbBeanFiller.fill(sheet, sheetLine, actualCol, null, element, "id");

        // Issr
        actualCol++;
        JaxbBeanFiller.fill(sheet, sheetLine, actualCol, null, element, "issr");

        // SchmeNm
        actualCol++;
        JaxbBeanFiller.fill(sheet, sheetLine, actualCol, null, element, "schmeNm");
}
}
