/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.galgo.boletador.importers.util;

import br.com.galgo.boletador.importers.converter.StringToXMLGregorianCalendarConverter;
import iso.std.iso._20022.tech.xsd.semt_003_001.ClassificationType2Choice;
import iso.std.iso._20022.tech.xsd.semt_003_001.FinancialInstrumentAttributes20;
import iso.std.iso._20022.tech.xsd.semt_003_001.FormOfSecurity1Code;
import iso.std.iso._20022.tech.xsd.semt_003_001.FormOfSecurity2Choice;
import iso.std.iso._20022.tech.xsd.semt_003_001.MarketIdentification1Choice;
import iso.std.iso._20022.tech.xsd.semt_003_001.MarketIdentification5;
import iso.std.iso._20022.tech.xsd.semt_003_001.MarketType2Choice;
import iso.std.iso._20022.tech.xsd.semt_003_001.MarketType5Code;
import iso.std.iso._20022.tech.xsd.semt_003_001.PreferenceToIncome1Code;
import iso.std.iso._20022.tech.xsd.semt_003_001.PreferenceToIncome2Choice;
import java.math.BigDecimal;
import java.text.ParseException;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.XMLGregorianCalendar;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author joyce.oliveira
 */
public class FinancialInstrumentAttributes {

    private static final Logger log = LoggerFactory.getLogger(FinancialInstrumentAttributes.class);

    /**
     *
     * @param atributo Objeto que contém os Atributos do Fundo
     * @param codMIC Código MIC
     * @param cdMercado Código de Tipo de Mercado (Balcao - OTCO ou Mercado Formal EXCH)
     * @param cdRegistro Forma de Registro (Portador - BEAR ou Nominativo - REGD)
     * @param preferenciaRenda Preferencia para Renda (Ordinária - ORDN ou Preferencial - PFRD)
     * @param cdCFI Código CFI
     * @param moeda Moeda de Denominação
     * @param dataVencimento
     * @param dataEmissao
     * @param taxaJuros
     * @param baseTaxaCupom
     * @throws ParseException
     * @throws DatatypeConfigurationException
     */
    public void geraFinancialInstrumentAttributes(FinancialInstrumentAttributes20 atributo,
            String codMIC, String cdMercado, String cdRegistro, String preferenciaRenda,
            String cdCFI, String moeda, String dataVencimento, String dataEmissao, String taxaJuros,
            String baseTaxaCupom)
            throws ParseException,
            DatatypeConfigurationException {

        log.trace("codMIC={}, cdMercado={}, ...", codMIC, cdMercado);

        StringToXMLGregorianCalendarConverter data = new StringToXMLGregorianCalendarConverter();

        //SET Código MIC
        MarketIdentification5 plcOfListg = atributo.getPlcOfListg();
        if (plcOfListg == null) {
            plcOfListg = new MarketIdentification5();
            atributo.setPlcOfListg(plcOfListg);
        }

        MarketIdentification1Choice id = plcOfListg.getId();
        if (id == null) {
            id = new MarketIdentification1Choice();
            plcOfListg.setId(id);
        }

        id.setMktIdrCd(codMIC);

        //SET Tipo Mercado
        MarketType2Choice tp = plcOfListg.getTp();
        if (tp == null) {
            tp = new MarketType2Choice();
            plcOfListg.setTp(tp);
        }

        if (cdMercado != null) {
            MarketType5Code codTipoMercado = MarketType5Code.fromValue(cdMercado);
            tp.setCd(codTipoMercado);
        }

        //SET Código Forma de Registro
        FormOfSecurity2Choice regnForm = atributo.getRegnForm();
        if (regnForm == null) {
            regnForm = new FormOfSecurity2Choice();
            atributo.setRegnForm(regnForm);
        }

        if (cdRegistro != null) {
            FormOfSecurity1Code codigoRegistro = FormOfSecurity1Code.fromValue(cdRegistro);
            regnForm.setCd(codigoRegistro);
        }

        //SET - Preferencia de Renda
        if (preferenciaRenda != null) {
            PreferenceToIncome2Choice prefToIncm = atributo.getPrefToIncm();
            if (prefToIncm == null) {
                prefToIncm = new PreferenceToIncome2Choice();
                atributo.setPrefToIncm(prefToIncm);
            }
            PreferenceToIncome1Code prefRenda = PreferenceToIncome1Code.fromValue(preferenciaRenda);
            prefToIncm.setCd(prefRenda);
        }

        ClassificationType2Choice clssfctnTp = atributo.getClssfctnTp();
        if (clssfctnTp == null) {
            clssfctnTp = new ClassificationType2Choice();
            atributo.setClssfctnTp(clssfctnTp);
        }

        clssfctnTp.setClssfctnFinInstrm(cdCFI);
        atributo.setDnmtnCcy(moeda);

        //SET - Data Vencimento - precisa corrigir o formato da data para apresentar aaaa-MM-dd
        if (dataVencimento != null) {

            XMLGregorianCalendar dtVcmt;
            dtVcmt = data.stringToXMLGregorianCalendarAno(dataVencimento);
            atributo.setMtrtyDt(dtVcmt);

            //SET - Data Emissão
            if (dataEmissao != null) {
                XMLGregorianCalendar dtEmissao;
                dtEmissao = data.stringToXMLGregorianCalendarAno(dataEmissao);
                atributo.setIsseDt(dtEmissao);
            }

            //SET - Taxa de Juros
            if (taxaJuros != null) {
                BigDecimal txJrs = new BigDecimal(taxaJuros);
                atributo.setIntrstRate(txJrs);
            }

            //SET - Base Taxa Indexação
            if (baseTaxaCupom != null) {
                BigDecimal bsTxCupom = new BigDecimal(baseTaxaCupom);
                atributo.setIndxRateBsis(bsTxCupom);
            }
        }

        // SET RegnForm com valor fixo REGD
        regnForm = new FormOfSecurity2Choice();
        atributo.setRegnForm(regnForm);
        regnForm.setCd(FormOfSecurity1Code.REGD);
    }
}
