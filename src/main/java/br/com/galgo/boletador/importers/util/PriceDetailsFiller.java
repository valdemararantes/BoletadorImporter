/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.galgo.boletador.importers.util;

import br.com.galgo.boletador.importers.ExcelUtils;
import br.com.galgo.boletador.importers.converter.StringToXMLGregorianCalendarConverter;
import com.galgo.utils.ApplicationException;
import iso.std.iso._20022.tech.xsd.semt_003_001.ActiveOrHistoricCurrencyAnd13DecimalAmount;
import iso.std.iso._20022.tech.xsd.semt_003_001.DateAndDateTimeChoice;
import iso.std.iso._20022.tech.xsd.semt_003_001.PriceInformation5;
import iso.std.iso._20022.tech.xsd.semt_003_001.PriceRateOrAmountOrUnknownChoice;
import iso.std.iso._20022.tech.xsd.semt_003_001.PriceValueType1Code;
import iso.std.iso._20022.tech.xsd.semt_003_001.TypeOfPrice11Code;
import iso.std.iso._20022.tech.xsd.semt_003_001.TypeOfPrice4Choice;
import iso.std.iso._20022.tech.xsd.semt_003_001.YieldedOrValueType1Choice;
import java.math.BigDecimal;
import java.text.ParseException;
import javax.xml.datatype.DatatypeConfigurationException;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.ss.usermodel.Sheet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author joyce.oliveira
 */
public class PriceDetailsFiller {

    private static final Logger log = LoggerFactory.getLogger(PriceDetailsFiller.class);

    /**
     * Preenche os dados de PriceDetailsFiller utilizando as 5 colunas, initialCol a initialCol + 4.
     * Os dados nas colunas devem ser:
     * <ul>
     * <li>Código do Preço
     * <li>PU da Posição
     * <li>Moeda do Preço
     * <li>e Tipo de Valor
     * <li>Data da Operação
     * </ul>
     *
     * @param pricDtls instância que será populada
     * @param sheet Planlha com os dados
     * @param rowNr Linha da planilha
     * @param initialCol Coluna inicial da planilha
     */
    public static void fill(PriceInformation5 pricDtls, Sheet sheet, int rowNr, int initialCol) {

        log.trace("sheetName={}, rownNr={}, initialCol={}",
                new Object[]{sheet.getSheetName(), rowNr, initialCol});

        try {
            int col = initialCol;

            String cdPreco = ExcelUtils.getCellAsString(sheet, rowNr, col);

            col++;
            String moeda = ExcelUtils.getCellAsString(sheet, rowNr, col);

            col++;
            String puPosicaoStr = ExcelUtils.getCellAsString(sheet, rowNr, col);
            BigDecimal puPosicao = null;
            try {
                if (StringUtils.isBlank(puPosicaoStr)) {
                    log.warn("A planilha " + sheet.getSheetName() + " possui a célula "
                            + ExcelUtils.getExcelColumnName(col) + "[" + col + "]" + "," + (rowNr
                            + 1) + "=" + puPosicaoStr + ". Não é um puPosicao válido.");
                } else {
                    puPosicao = new BigDecimal(puPosicaoStr);
                }
            } catch (NumberFormatException e) {
                log.warn("A planilha " + sheet.getSheetName()
                        + " possui a célula " + ExcelUtils.getExcelColumnName(col) + "[" + col + "]"
                        + "," + (rowNr + 1) + "=" + puPosicaoStr + ". Não é um puPosicao válido.");
            }

            col++;
            String tipoValor = ExcelUtils.getCellAsString(sheet, rowNr, col);

            col++;
            String dtOperacao = ExcelUtils.getCellAsString(sheet, rowNr, col);

            geraPriceDetails(pricDtls, cdPreco, puPosicao, moeda, tipoValor, dtOperacao);

        } catch (Exception e) {
            log.error("Erro ao obter dados da planilha " + sheet.getSheetName() + ", rownNr="
                    + rowNr + ", initialCol={}" + initialCol, e);
        }

    }

    /**
     *
     * @param detalhesPreco Subseção para indicar tipos de preço
     * @param cdPreco Código do Preço
     * @param puPosicao PU da Posição
     * @param moeda Moeda do Preço
     * @param tipoValor Tipo de Valor
     * @param dataOperacao
     * @throws ParseException
     * @throws DatatypeConfigurationException
     */
    private static void geraPriceDetails(PriceInformation5 detalhesPreco, String cdPreco,
            BigDecimal puPosicao, String moeda, String tipoValor, String dataOperacao) throws
            ParseException, DatatypeConfigurationException {

        log.trace("cdPreco={}, puPosicao={}, ...", cdPreco, puPosicao);

        TypeOfPrice4Choice tp = detalhesPreco.getTp();
        if (tp == null) {
            tp = new TypeOfPrice4Choice();
            detalhesPreco.setTp(tp);
        }

        if (cdPreco != null) {
            TypeOfPrice11Code codigoPreco = TypeOfPrice11Code.fromValue(cdPreco);
            tp.setCd(codigoPreco);
        }

        PriceRateOrAmountOrUnknownChoice val = detalhesPreco.getVal();
        if (val == null) {
            val = new PriceRateOrAmountOrUnknownChoice();
            detalhesPreco.setVal(val);
        }

        if (puPosicao != null) {
            ActiveOrHistoricCurrencyAnd13DecimalAmount valorPuPosicao = new ActiveOrHistoricCurrencyAnd13DecimalAmount();
            val.setAmt(valorPuPosicao);
            valorPuPosicao.setCcy(moeda);
            valorPuPosicao.setValue(puPosicao);
        }

        YieldedOrValueType1Choice valTp = detalhesPreco.getValTp();
        if (valTp == null) {
            valTp = new YieldedOrValueType1Choice();
            detalhesPreco.setValTp(valTp);
        }

        if (tipoValor != null) {
            PriceValueType1Code tipoValTp = PriceValueType1Code.fromValue(tipoValor);
            valTp.setValTp(tipoValTp);
        }

        if (dataOperacao != null) {
            DateAndDateTimeChoice qtnDt = detalhesPreco.getQtnDt();
            if (qtnDt == null) {
                qtnDt = new DateAndDateTimeChoice();
                detalhesPreco.setQtnDt(qtnDt);
            }

            qtnDt.setDt(StringToXMLGregorianCalendarConverter.stringToXMLGregorianCalendarAno(
                    dataOperacao));
        }
    }

}
