/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.galgo.boletador.importers;

import br.com.galgo.boletador.importers.converter.StringToXMLGregorianCalendarConverter;
import com.galgo.utils.ApplicationException;
import com.galgo.utils.XMLGregorianCalendarConversionUtil;
import com.sistemagalgo.schemaposicaoativos.BsnsMsgComplexType;
import com.sistemagalgo.schemaposicaoativos.GalgoAssBalStmtComplexType;
import com.sistemagalgo.schemaposicaoativos.GalgoHdrComplexType;
import iso.std.iso._20022.tech.xsd.head_001_001.BusinessApplicationHeaderV01;
import iso.std.iso._20022.tech.xsd.head_001_001.GenericOrganisationIdentification1;
import iso.std.iso._20022.tech.xsd.head_001_001.OrganisationIdentification7;
import iso.std.iso._20022.tech.xsd.head_001_001.OrganisationIdentificationSchemeName1Choice;
import iso.std.iso._20022.tech.xsd.head_001_001.Party10Choice;
import iso.std.iso._20022.tech.xsd.head_001_001.Party9Choice;
import iso.std.iso._20022.tech.xsd.head_001_001.PartyIdentification42;
import iso.std.iso._20022.tech.xsd.semt_003_001.ActiveOrHistoricCurrencyAnd13DecimalAmount;
import iso.std.iso._20022.tech.xsd.semt_003_001.ActiveOrHistoricCurrencyAndAmount;
import iso.std.iso._20022.tech.xsd.semt_003_001.AdditionalBalanceInformation6;
import iso.std.iso._20022.tech.xsd.semt_003_001.AggregateBalanceInformation13;
import iso.std.iso._20022.tech.xsd.semt_003_001.AmountAndDirection6;
import iso.std.iso._20022.tech.xsd.semt_003_001.Balance1;
import iso.std.iso._20022.tech.xsd.semt_003_001.BalanceAmounts1;
import iso.std.iso._20022.tech.xsd.semt_003_001.BalanceQuantity4Choice;
import iso.std.iso._20022.tech.xsd.semt_003_001.DateAndDateTimeChoice;
import iso.std.iso._20022.tech.xsd.semt_003_001.Document;
import iso.std.iso._20022.tech.xsd.semt_003_001.EventFrequency4Code;
import iso.std.iso._20022.tech.xsd.semt_003_001.FinancialInstrumentQuantity1Choice;
import iso.std.iso._20022.tech.xsd.semt_003_001.Frequency4Choice;
import iso.std.iso._20022.tech.xsd.semt_003_001.GenericIdentification13;
import iso.std.iso._20022.tech.xsd.semt_003_001.GenericIdentification19;
import iso.std.iso._20022.tech.xsd.semt_003_001.GenericIdentification20;
import iso.std.iso._20022.tech.xsd.semt_003_001.IdentificationSource3Choice;
import iso.std.iso._20022.tech.xsd.semt_003_001.Number3Choice;
import iso.std.iso._20022.tech.xsd.semt_003_001.OtherIdentification1;
import iso.std.iso._20022.tech.xsd.semt_003_001.Pagination;
import iso.std.iso._20022.tech.xsd.semt_003_001.PartyIdentification36Choice;
import iso.std.iso._20022.tech.xsd.semt_003_001.PartyIdentification49Choice;
import iso.std.iso._20022.tech.xsd.semt_003_001.PriceInformation5;
import iso.std.iso._20022.tech.xsd.semt_003_001.PriceRateOrAmountOrUnknownChoice;
import iso.std.iso._20022.tech.xsd.semt_003_001.PriceValueType1Code;
import iso.std.iso._20022.tech.xsd.semt_003_001.PurposeCode1Choice;
import iso.std.iso._20022.tech.xsd.semt_003_001.Quantity6Choice;
import iso.std.iso._20022.tech.xsd.semt_003_001.SecuritiesAccount11;
import iso.std.iso._20022.tech.xsd.semt_003_001.SecuritiesBalanceAccountingReportV04;
import iso.std.iso._20022.tech.xsd.semt_003_001.SecuritiesBalanceType12Code;
import iso.std.iso._20022.tech.xsd.semt_003_001.SecuritiesBalanceType7Code;
import iso.std.iso._20022.tech.xsd.semt_003_001.SecurityIdentification14;
import iso.std.iso._20022.tech.xsd.semt_003_001.ShortLong1Code;
import iso.std.iso._20022.tech.xsd.semt_003_001.Statement20;
import iso.std.iso._20022.tech.xsd.semt_003_001.StatementBasis1Code;
import iso.std.iso._20022.tech.xsd.semt_003_001.StatementBasis3Choice;
import iso.std.iso._20022.tech.xsd.semt_003_001.StatementUpdateType1Code;
import iso.std.iso._20022.tech.xsd.semt_003_001.SubBalanceInformation6;
import iso.std.iso._20022.tech.xsd.semt_003_001.SubBalanceQuantity3Choice;
import iso.std.iso._20022.tech.xsd.semt_003_001.SubBalanceType5Choice;
import iso.std.iso._20022.tech.xsd.semt_003_001.SubBalanceType6Choice;
import iso.std.iso._20022.tech.xsd.semt_003_001.TotalValueInPageAndStatement2;
import iso.std.iso._20022.tech.xsd.semt_003_001.TypeOfPrice11Code;
import iso.std.iso._20022.tech.xsd.semt_003_001.TypeOfPrice4Choice;
import iso.std.iso._20022.tech.xsd.semt_003_001.UpdateType2Choice;
import iso.std.iso._20022.tech.xsd.semt_003_001.YieldedOrValueType1Choice;
import java.math.BigDecimal;
import java.text.ParseException;
import java.util.List;
import javax.xml.datatype.XMLGregorianCalendar;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author valdemar.arantes
 */
public class FundoImporter extends SheetImporter {

    private static final Logger log = LoggerFactory.getLogger(FundoImporter.class);
    private static final int HEADER_ROW_NUMBER = 1;
    private static final int DATA_ROW_NUMBER = HEADER_ROW_NUMBER + 1;
    StringToXMLGregorianCalendarConverter converterStringToGregCalend;
    XMLGregorianCalendar xmlGregCalendToString;

    public FundoImporter(GalgoAssBalStmtComplexType glg, Sheet sheet) {
        super(glg, sheet);
    }

    @Override
    public void importSheet() {
        log.debug("Importando o fundo");
        Row row = sheet.getRow(DATA_ROW_NUMBER);
        if (row == null) {
            log.info("Não há fundos cadastrados.");
            return;
        }

        // GalgoHdr
        GalgoHdrComplexType galgoHdr = glg.getGalgoHdr();
        if (galgoHdr == null) {
            galgoHdr = new GalgoHdrComplexType();
            glg.setGalgoHdr(galgoHdr);
        }

        //SET IdMessenger
        galgoHdr.setIdMsgSender(ExcelUtils.getCellAsString(row, 0));

        //BsnsMsg
        List<BsnsMsgComplexType> galgoBsnsMsgList = glg.getBsnsMsg();
        BsnsMsgComplexType bsnsMsg = new BsnsMsgComplexType();
        galgoBsnsMsgList.add(bsnsMsg);

        //BAH
        BusinessApplicationHeaderV01 appHdr = bsnsMsg.getAppHdr();
        if (appHdr == null) {
            appHdr = new BusinessApplicationHeaderV01();
            bsnsMsg.setAppHdr(appHdr);
        }

        //BAH - Fr
        Party9Choice fr = appHdr.getFr();
        if (fr == null) {
            fr = new Party9Choice();
            appHdr.setFr(fr);
        }

        //BAH - Fr - OrgId
        PartyIdentification42 orgId = fr.getOrgId();
        if (orgId == null) {
            orgId = new PartyIdentification42();
            fr.setOrgId(orgId);
        }

        //SET Name
        orgId.setNm(ExcelUtils.getCellAsString(row, 1));

        //BAH - Fr - OrgId - Id
        Party10Choice id = orgId.getId();
        if (id == null) {
            id = new Party10Choice();
            orgId.setId(id);
        }
        //BAH - Fr - OrgId - Id - OrgId
        OrganisationIdentification7 orgIdBAH = id.getOrgId();
        if (orgIdBAH == null) {
            orgIdBAH = new OrganisationIdentification7();
            id.setOrgId(orgIdBAH);
        }

        //BAH - Fr - OrgId - Id - OrgId - Othr
        List<GenericOrganisationIdentification1> othrList = orgIdBAH.getOthr();
        GenericOrganisationIdentification1 othr = new GenericOrganisationIdentification1();
        othrList.add(othr);

        //SET - Id
        othr.setId(ExcelUtils.getCellAsString(row, 2));

        //BAH - Fr - OrgId - Id - OrgId - Othr - SchmeNm
        OrganisationIdentificationSchemeName1Choice schmeNm = othr.getSchmeNm();
        if (schmeNm == null) {
            schmeNm = new OrganisationIdentificationSchemeName1Choice();
            othr.setSchmeNm(schmeNm);
        }
        //BAH - To
        Party9Choice toBAH = appHdr.getTo();
        if (toBAH == null) {
            toBAH = new Party9Choice();
            appHdr.setTo(toBAH);
        }

        //BAH - To - OrgId
        PartyIdentification42 toOrgId = toBAH.getOrgId();
        if (toOrgId == null) {
            toOrgId = new PartyIdentification42();
            toBAH.setOrgId(toOrgId);
        }

        toOrgId.setNm(ExcelUtils.getCellAsString(row, 6));
        toOrgId.setCtryOfRes(ExcelUtils.getCellAsString(row, 7));

        //SET CNPJ
        schmeNm.setCd(ExcelUtils.getCellAsString(row, 3));
        //SET Emissor
        othr.setIssr(ExcelUtils.getCellAsString(row, 4));
        //SET Pais da Entidade
        orgId.setCtryOfRes(ExcelUtils.getCellAsString(row, 5));
        //SET Identificador Unico da Mensagem
        appHdr.setBizMsgIdr(ExcelUtils.getCellAsString(row, 8));
        //SET Versao ISO da Mensagem
        appHdr.setMsgDefIdr(ExcelUtils.getCellAsString(row, 9));
        //SET Nome do Serviço
        appHdr.setBizSvc(ExcelUtils.getCellAsString(row, 10));

        //SET Data de Criação
        appHdr.setCreDt(ExcelUtils.getCellAsXMLGregCalendarAAAAMMDDTZ(ExcelUtils.
                getCellAsString(row, 11)));

        //Document
        Document document = bsnsMsg.getDocument();
        if (document == null) {
            document = new Document();
            bsnsMsg.setDocument(document);
        }

        //SctiesBalAcctgRpt
        SecuritiesBalanceAccountingReportV04 sctiesBalAcctgRpt = document.getSctiesBalAcctgRpt();
        if (sctiesBalAcctgRpt == null) {
            sctiesBalAcctgRpt = new SecuritiesBalanceAccountingReportV04();
            document.setSctiesBalAcctgRpt(sctiesBalAcctgRpt);
        }

        //Pagination
        Pagination pgntn = sctiesBalAcctgRpt.getPgntn();
        if (pgntn == null) {
            pgntn = new Pagination();
            sctiesBalAcctgRpt.setPgntn(pgntn);
        }

        //SET Page Number
        pgntn.setPgNb(ExcelUtils.getCellAsString(row, 12));
        //SET Last Page Indicator
        pgntn.setLastPgInd(Boolean.parseBoolean(ExcelUtils.getCellAsString(row, 13)));

        //SET StmtGnlDtls
        Statement20 stmtGnlDtls = sctiesBalAcctgRpt.getStmtGnlDtls();
        if (stmtGnlDtls == null) {
            stmtGnlDtls = new Statement20();
            sctiesBalAcctgRpt.setStmtGnlDtls(stmtGnlDtls);
        }

        if (ExcelUtils.getCellAsString(row, 14) != null) {
            Number3Choice rptNbS = stmtGnlDtls.getRptNb();
            if (rptNbS == null) {
                rptNbS = new Number3Choice();
                stmtGnlDtls.setRptNb(rptNbS);
                rptNbS.setShrt(ExcelUtils.getCellAsString(row, 14));
            }
        } else if (ExcelUtils.getCellAsString(row, 15) != null) {
            Number3Choice rptNbL = stmtGnlDtls.getRptNb();
            if (rptNbL == null) {
                rptNbL = new Number3Choice();
                stmtGnlDtls.setRptNb(rptNbL);
                rptNbL.setLng(ExcelUtils.getCellAsString(row, 15));
            }
        }

        stmtGnlDtls.setQryRef(ExcelUtils.getCellAsString(row, 16));

        if (ExcelUtils.getCellAsString(row, 17) != null) {
            //SET Justificativa, se campo foi preenchido
            stmtGnlDtls.setStmtId(ExcelUtils.getCellAsString(row, 17));
        }

        //StmtDtTm
        DateAndDateTimeChoice stmtDtTm = stmtGnlDtls.getStmtDtTm();
        if (stmtDtTm == null) {
            stmtDtTm = new DateAndDateTimeChoice();
            stmtGnlDtls.setStmtDtTm(stmtDtTm);
        }

        try {
            //Set Data Informação
            stmtDtTm.setDt(XMLGregorianCalendarConversionUtil.asXMLGregorianCalendar_Date(
                    ExcelUtils.getCellAsString(row,
                            18), "dd-MM-yyyy"));
        } catch (ParseException ex) {
            throw new ApplicationException("A coluna 18 não está com a data no formato aaaa-mm-dd");
        }

        //SET Frequencia de Envio
        if (ExcelUtils.getCellAsString(row, 19) != null) {
            try {
                Frequency4Choice frqcy = stmtGnlDtls.getFrqcy();
                if (frqcy == null) {
                    frqcy = new Frequency4Choice();
                    stmtGnlDtls.setFrqcy(frqcy);
                }

                EventFrequency4Code teste = EventFrequency4Code.fromValue(ExcelUtils.
                        getCellAsString(row, 19));
                frqcy.setCd(teste);
            } catch (IllegalArgumentException e) {
                log.debug(
                        "ALERTA: Verificar o campo Frequencia de Envio: <Document>"
                        + "<SctiesBalAcctgRpt><StmtGnlDtls><Frqcy>");
            }
        }

        //SET Tipo de Atualização
        if (ExcelUtils.getCellAsString(row, 20) != null) {
            try {
                UpdateType2Choice updTp = stmtGnlDtls.getUpdTp();
                if (updTp == null) {
                    updTp = new UpdateType2Choice();
                    stmtGnlDtls.setUpdTp(updTp);
                }

                StatementUpdateType1Code sttmtUpd1Cd = StatementUpdateType1Code.fromValue(
                        ExcelUtils.getCellAsString(row, 20));
                updTp.setCd(sttmtUpd1Cd);
            } catch (IllegalArgumentException e) {
                log.debug("ALERTA : Verificar o campo Tipo de Atualização: <Document>"
                        + "<SctiesBalAcctgRpt><StmtGnlDtls><UpdTp>");
            }
        }

        //SET Tipo de Informação do Arquivo
        if (ExcelUtils.getCellAsString(row, 21) != null) {
            try {
                StatementBasis3Choice stmtBsis = stmtGnlDtls.getStmtBsis();
                if (stmtBsis == null) {
                    stmtBsis = new StatementBasis3Choice();
                    stmtGnlDtls.setStmtBsis(stmtBsis);
                }

                StatementBasis1Code sttmtBs1Cd = StatementBasis1Code.fromValue(ExcelUtils.
                        getCellAsString(row, 21));
                stmtBsis.setCd(sttmtBs1Cd);
            } catch (IllegalArgumentException e) {
                log.debug("ALERTA : Verificar o campo Tipo de Informação do Arquivo: <Document>"
                        + "<SctiesBalAcctgRpt><StmtGnlDtls><StmtBsis>");
            }
        }

        //SET Indicador de Atividade
        if (ExcelUtils.getCellAsString(row, 22) != null) {
            stmtGnlDtls.setActvtyInd(Boolean.parseBoolean(ExcelUtils.getCellAsString(row, 22)));
        }

        //SET Indicador de Auditoria
        if (ExcelUtils.getCellAsString(row, 23) != null) {
            stmtGnlDtls.setAudtdInd(Boolean.parseBoolean(ExcelUtils.getCellAsString(row, 23)));
        }

        //SET Indicador de Subconta de Custódia
        if (ExcelUtils.getCellAsString(row, 24) != null) {
            stmtGnlDtls.setSubAcctInd(Boolean.parseBoolean(ExcelUtils.getCellAsString(row, 24)));
        }

        //SET Administrador - AcctOwnr
        PartyIdentification36Choice acctOwnr = sctiesBalAcctgRpt.getAcctOwnr();
        if (acctOwnr == null) {
            acctOwnr = new PartyIdentification36Choice();
            sctiesBalAcctgRpt.setAcctOwnr(acctOwnr);
        }

        GenericIdentification19 prtyIdAcctOwnr = acctOwnr.getPrtryId();
        if (prtyIdAcctOwnr == null) {
            prtyIdAcctOwnr = new GenericIdentification19();
            acctOwnr.setPrtryId(prtyIdAcctOwnr);
        }

        if (ExcelUtils.getCellAsString(row, 25) != null) {
            prtyIdAcctOwnr.setId(ExcelUtils.getCellAsString(row, 25));
        }
        if (ExcelUtils.getCellAsString(row, 26) != null) {
            prtyIdAcctOwnr.setIssr(ExcelUtils.getCellAsString(row, 26));
        }
        if (ExcelUtils.getCellAsString(row, 27) != null) {
            prtyIdAcctOwnr.setSchmeNm(ExcelUtils.getCellAsString(row, 27));
        }

        //SET Gestor - AcctServicer
        PartyIdentification49Choice acctSvcr = sctiesBalAcctgRpt.getAcctSvcr();
        if (acctSvcr == null) {
            acctSvcr = new PartyIdentification49Choice();
            sctiesBalAcctgRpt.setAcctSvcr(acctSvcr);
        }
        GenericIdentification19 prtyIdAccSvcr = acctSvcr.getPrtryId();
        if (prtyIdAccSvcr == null) {
            prtyIdAccSvcr = new GenericIdentification19();
            acctSvcr.setPrtryId(prtyIdAccSvcr);
        }

        if (ExcelUtils.getCellAsString(row, 28) != null) {
            prtyIdAccSvcr.setId(ExcelUtils.getCellAsString(row, 28));
        }
        if (ExcelUtils.getCellAsString(row, 29) != null) {
            prtyIdAccSvcr.setIssr(ExcelUtils.getCellAsString(row, 29));
        }
        if (ExcelUtils.getCellAsString(row, 30) != null) {
            prtyIdAccSvcr.setSchmeNm(ExcelUtils.getCellAsString(row, 30));
        }

        //SET Custodiante - SfkpgAcct
        SecuritiesAccount11 sfkpgAcct = sctiesBalAcctgRpt.getSfkpgAcct();
        if (sfkpgAcct == null) {
            sfkpgAcct = new SecuritiesAccount11();
            sctiesBalAcctgRpt.setSfkpgAcct(sfkpgAcct);
        }

        if (ExcelUtils.getCellAsString(row, 31) != null) {
            sfkpgAcct.setId(ExcelUtils.getCellAsString(row, 31));
        }

        if (ExcelUtils.getCellAsString(row, 32) != null) {
            PurposeCode1Choice tpSfkAcct = sfkpgAcct.getTp();
            if (tpSfkAcct == null) {
                tpSfkAcct = new PurposeCode1Choice();
                sfkpgAcct.setTp(tpSfkAcct);
            }
            GenericIdentification13 prtrySfAcct = tpSfkAcct.getPrtry();
            if (prtrySfAcct == null) {
                prtrySfAcct = new GenericIdentification13();
                tpSfkAcct.setPrtry(prtrySfAcct);
            }

            prtrySfAcct.setId(ExcelUtils.getCellAsString(row, 32));
            prtrySfAcct.setIssr(ExcelUtils.getCellAsString(row, 33));
            sfkpgAcct.setNm(ExcelUtils.getCellAsString(row, 34));

        }

        //-----------------------SET Fundo--------------------------------
        List<AggregateBalanceInformation13> balForAcctList = sctiesBalAcctgRpt.getBalForAcct();
        AggregateBalanceInformation13 balForAcct = new AggregateBalanceInformation13();
        balForAcctList.add(balForAcct);

        //Identificação do Fundo -> FinInstrmId
        SecurityIdentification14 finInstrmId = balForAcct.getFinInstrmId();
        if (finInstrmId == null) {
            finInstrmId = new SecurityIdentification14();
            balForAcct.setFinInstrmId(finInstrmId);
        }

        //SET ISIN
        if (ExcelUtils.getCellAsString(row, 35) != null) {
            finInstrmId.setISIN(ExcelUtils.getCellAsString(row, 35));
            log.info("Estamos na row : " + row.getRowNum());
        }

        //SET Lista de Identificações
        for (int contId = row.getRowNum(); ExcelUtils.getCellAsString(this.sheet, contId, 36)
                != null;
                contId++) {
            List<OtherIdentification1> othrIdList = finInstrmId.getOthrId();
            OtherIdentification1 othrID = new OtherIdentification1();
            othrIdList.add(othrID);
            othrID.setId(ExcelUtils.getCellAsString(this.sheet, contId, 36));

            IdentificationSource3Choice tp = othrID.getTp();
            if (tp == null) {
                tp = new IdentificationSource3Choice();
                othrID.setTp(tp);
            }

            if (ExcelUtils.getCellAsString(this.sheet, contId, 37) != null) {
                tp.setCd(ExcelUtils.getCellAsString(this.sheet, contId, 37));
            }

            if (ExcelUtils.getCellAsString(this.sheet, contId, 38) != null) {
                tp.setPrtry(ExcelUtils.getCellAsString(this.sheet, contId, 38));
            }
        }

        if (ExcelUtils.getCellAsString(row, 39) != null) {
            finInstrmId.setDesc(ExcelUtils.getCellAsString(row, 39));
        }

        //AggtBal
        Balance1 aggtBal = balForAcct.getAggtBal();

        if (aggtBal == null) {
            aggtBal = new Balance1();
            balForAcct.setAggtBal(aggtBal);
        }

        //SET Indicador de comprado ou vendido
        if (ExcelUtils.getCellAsString(row, 40) != null) {
            ShortLong1Code shortLong1Cd = ShortLong1Code.fromValue(ExcelUtils.getCellAsString(row,
                    40));
            aggtBal.setShrtLngInd(shortLong1Cd);
        }

        if (ExcelUtils.getCellAsString(row, 41) != null) {
            BalanceQuantity4Choice qty = aggtBal.getQty();
            if (qty == null) {
                qty = new BalanceQuantity4Choice();
                aggtBal.setQty(qty);
            }

            Quantity6Choice qtyII = qty.getQty();
            if (qtyII == null) {
                qtyII = new Quantity6Choice();
                qty.setQty(qtyII);
            }

            FinancialInstrumentQuantity1Choice qtyIII = qtyII.getQty();
            if (qtyIII == null) {
                qtyIII = new FinancialInstrumentQuantity1Choice();
                qtyII.setQty(qtyIII);
            }

            String qntCotas = ExcelUtils.getCellAsString(row, 41);
            BigDecimal qntCotasBigDecimal = new BigDecimal(qntCotas);
            qtyIII.setUnit(qntCotasBigDecimal);
        }

        //PricDtls
        List<PriceInformation5> pricDtlsList = balForAcct.getPricDtls();
        PriceInformation5 pricDtls = new PriceInformation5();
        pricDtlsList.add(pricDtls);

        TypeOfPrice4Choice tpPricDtls = pricDtls.getTp();
        if (tpPricDtls == null) {
            tpPricDtls = new TypeOfPrice4Choice();
            pricDtls.setTp(tpPricDtls);
        }

        tpPricDtls.setCd(TypeOfPrice11Code.fromValue(ExcelUtils.getCellAsString(row, 42)));

        PriceRateOrAmountOrUnknownChoice val = pricDtls.getVal();
        if (val == null) {
            val = new PriceRateOrAmountOrUnknownChoice();
            pricDtls.setVal(val);
        }

        ActiveOrHistoricCurrencyAnd13DecimalAmount vlrCota = new ActiveOrHistoricCurrencyAnd13DecimalAmount();
        String vlrCotasString = ExcelUtils.getCellAsString(row, 43);
        BigDecimal valorCotas = new BigDecimal(vlrCotasString);
        vlrCota.setValue(valorCotas);
        vlrCota.setCcy(ExcelUtils.getCellAsString(row, 44));
        val.setAmt(vlrCota);

        YieldedOrValueType1Choice valTpPrcDtls = pricDtls.getValTp();
        if (valTpPrcDtls == null) {
            valTpPrcDtls = new YieldedOrValueType1Choice();
            pricDtls.setValTp(valTpPrcDtls);
        }

        PriceValueType1Code tipoValor = PriceValueType1Code.fromValue(ExcelUtils.
                getCellAsString(row, 45));
        valTpPrcDtls.setValTp(tipoValor);

        //AcctBaseCcyAmts
        BalanceAmounts1 acctBaseCcyAmts = balForAcct.getAcctBaseCcyAmts();
        if (acctBaseCcyAmts == null) {
            acctBaseCcyAmts = new BalanceAmounts1();
            balForAcct.setAcctBaseCcyAmts(acctBaseCcyAmts);
        }
        AmountAndDirection6 hldgVal = acctBaseCcyAmts.getHldgVal();
        if (hldgVal == null) {
            hldgVal = new AmountAndDirection6();
            acctBaseCcyAmts.setHldgVal(hldgVal);
        }

        ActiveOrHistoricCurrencyAndAmount vlrAtivos = new ActiveOrHistoricCurrencyAndAmount();
        String vlrAtivosString = ExcelUtils.getCellAsString(row, 46);
        BigDecimal valorAtivos = new BigDecimal(vlrAtivosString);
        vlrAtivos.setValue(valorAtivos);
        vlrAtivos.setCcy(ExcelUtils.getCellAsString(row, 47));
        hldgVal.setAmt(vlrAtivos);
        hldgVal.setSgn(Boolean.parseBoolean(ExcelUtils.getCellAsString(row, 48)));

        List<SubBalanceInformation6> balBrkdwnList = balForAcct.getBalBrkdwn();
        //SET lista de descrição de quantidades (Créditos, Débitos, Juros, etc)
        for (int actualRow = row.getRowNum(); ExcelUtils.getCellAsString(this.sheet, actualRow, 49)
                != null
                || ExcelUtils.getCellAsString(this.sheet, actualRow, 50) != null; actualRow++) {

            SubBalanceInformation6 balBrkdwn = new SubBalanceInformation6();
            balBrkdwnList.add(balBrkdwn);

            SubBalanceType5Choice subBalTp = balBrkdwn.getSubBalTp();
            if (subBalTp == null) {
                subBalTp = new SubBalanceType5Choice();
                balBrkdwn.setSubBalTp(subBalTp);
            }

            //Código
            if (null != ExcelUtils.getCellAsString(this.sheet, actualRow, 49)) {
                SecuritiesBalanceType12Code codSubSaldo = SecuritiesBalanceType12Code.fromValue(
                        ExcelUtils.getCellAsString(this.sheet, actualRow, 49));
                subBalTp.setCd(codSubSaldo);
            }

            //Identificação Proprietária
            if (null != ExcelUtils.getCellAsString(this.sheet, actualRow, 50)) {
                GenericIdentification20 prtryBrkDwn = subBalTp.getPrtry();
                if (prtryBrkDwn == null) {
                    prtryBrkDwn = new GenericIdentification20();
                    subBalTp.setPrtry(prtryBrkDwn);
                }

                prtryBrkDwn.setId(ExcelUtils.getCellAsString(this.sheet, actualRow, 50));
                prtryBrkDwn.setIssr(ExcelUtils.getCellAsString(this.sheet, actualRow, 51));
                prtryBrkDwn.setSchmeNm(ExcelUtils.getCellAsString(this.sheet, actualRow, 52));
            }

            SubBalanceQuantity3Choice balBrkdwnQty = balBrkdwn.getQty();
            if (balBrkdwnQty == null) {
                balBrkdwnQty = new SubBalanceQuantity3Choice();
                balBrkdwn.setQty(balBrkdwnQty);
            }

            FinancialInstrumentQuantity1Choice balBrkdwnQtyQty = balBrkdwnQty.getQty();
            if (balBrkdwnQtyQty == null) {
                balBrkdwnQtyQty = new FinancialInstrumentQuantity1Choice();
                balBrkdwnQty.setQty(balBrkdwnQtyQty);
            }

            BigDecimal vlrFinTotal = new BigDecimal(ExcelUtils.getCellAsString(this.sheet, actualRow,
                    53));
            balBrkdwnQtyQty.setFaceAmt(vlrFinTotal);

            //Lista indicadora do valor financeiro total
            List<AdditionalBalanceInformation6> addtlBalBrkdwnDtlsLst = balBrkdwn.
                    getAddtlBalBrkdwnDtls();

            for (int contAdd = actualRow; ExcelUtils.getCellAsString(this.sheet, contAdd, 54) != null
                    || ExcelUtils.getCellAsString(this.sheet, contAdd, 55) != null && row.
                    getRowNum() != 2; contAdd++) {

                AdditionalBalanceInformation6 addtlBalBrkdwnDtls = new AdditionalBalanceInformation6();

                addtlBalBrkdwnDtlsLst.add(addtlBalBrkdwnDtls);
                SubBalanceType6Choice addSubBalTp = addtlBalBrkdwnDtls.getSubBalTp();
                if (addSubBalTp == null) {
                    addSubBalTp = new SubBalanceType6Choice();
                    addtlBalBrkdwnDtls.setSubBalTp(addSubBalTp);
                }

                if (ExcelUtils.getCellAsString(this.sheet, contAdd, 54) != null) {
                    addSubBalTp.setCd(SecuritiesBalanceType7Code.fromValue(ExcelUtils.
                            getCellAsString(this.sheet, contAdd, 54)));
                }

                if (ExcelUtils.getCellAsString(this.sheet, contAdd, 55) != null) {
                    GenericIdentification20 addPrtry = addSubBalTp.getPrtry();
                    if (addPrtry == null) {
                        addPrtry = new GenericIdentification20();
                        addSubBalTp.setPrtry(addPrtry);
                    }

                    addPrtry.setId(ExcelUtils.getCellAsString(this.sheet, contAdd, 55));
                    addPrtry.setIssr(ExcelUtils.getCellAsString(this.sheet, contAdd, 56));
                    addPrtry.setSchmeNm(ExcelUtils.getCellAsString(this.sheet, contAdd, 57));
                }
                //Set Qty
                SubBalanceQuantity3Choice addQty = addtlBalBrkdwnDtls.getQty();
                if (addQty == null) {
                    addQty = new SubBalanceQuantity3Choice();
                    addtlBalBrkdwnDtls.setQty(addQty);
                }

                FinancialInstrumentQuantity1Choice addQtyQty = addQty.getQty();
                if (addQtyQty == null) {
                    addQtyQty = new FinancialInstrumentQuantity1Choice();
                    addQty.setQty(addQtyQty);
                }

                BigDecimal vlrAddInf = new BigDecimal(ExcelUtils.
                        getCellAsString(this.sheet, contAdd, 58));
                addQtyQty.setFaceAmt(vlrAddInf);

                actualRow++;

            }

            //QuantiasTotaisNaMoedaBaseDaConta - AcctBaseCcyTtlAmts
            TotalValueInPageAndStatement2 acctBaseCcyTtlAmts = sctiesBalAcctgRpt.
                    getAcctBaseCcyTtlAmts();

            if (acctBaseCcyTtlAmts == null) {
                acctBaseCcyTtlAmts = new TotalValueInPageAndStatement2();
                sctiesBalAcctgRpt.setAcctBaseCcyTtlAmts(acctBaseCcyTtlAmts);
            }

            //ValorTotalDaPágina - TtlHldgsValOfPg
            if (ExcelUtils.getCellAsString(row, 59) != null) {
                AmountAndDirection6 ttlHldgsValOfPg = acctBaseCcyTtlAmts.getTtlHldgsValOfPg();
                if (ttlHldgsValOfPg == null) {
                    ttlHldgsValOfPg = new AmountAndDirection6();
                    acctBaseCcyTtlAmts.setTtlHldgsValOfPg(ttlHldgsValOfPg);
                }

                ActiveOrHistoricCurrencyAndAmount vlrTtlPag = new ActiveOrHistoricCurrencyAndAmount();
                String valorTotPag = ExcelUtils.getCellAsString(row, 59);
                BigDecimal valorPg = new BigDecimal(valorTotPag);
                vlrTtlPag.setValue(valorPg);
                vlrTtlPag.setCcy(ExcelUtils.getCellAsString(row, 60));
                ttlHldgsValOfPg.setAmt(vlrTtlPag);
                ttlHldgsValOfPg.setSgn(Boolean.parseBoolean(ExcelUtils.getCellAsString(row, 61)));
            }

            if (ExcelUtils.getCellAsString(row, 62) != null) {
                AmountAndDirection6 ttlHldgsValOfStmt = acctBaseCcyTtlAmts.getTtlHldgsValOfStmt();
                if (ttlHldgsValOfStmt == null) {
                    ttlHldgsValOfStmt = new AmountAndDirection6();
                    acctBaseCcyTtlAmts.setTtlHldgsValOfStmt(ttlHldgsValOfStmt);
                }

                ActiveOrHistoricCurrencyAndAmount vlrTtlCart = new ActiveOrHistoricCurrencyAndAmount();
                String valorTotCart = ExcelUtils.getCellAsString(row, 62);
                BigDecimal valorCart = new BigDecimal(valorTotCart);
                vlrTtlCart.setValue(valorCart);
                vlrTtlCart.setCcy(ExcelUtils.getCellAsString(row, 63));
                ttlHldgsValOfStmt.setAmt(vlrTtlCart);
                ttlHldgsValOfStmt.setSgn(Boolean.parseBoolean(ExcelUtils.getCellAsString(row, 64)));
            }

        }

    }
}
