/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.galgo.boletador.importers.util;

import br.anbimacom.balanceforsubaccountbrazil.BalForSubAcctBrData;
import br.com.galgo.boletador.importers.ExcelUtils;
import org.apache.poi.ss.usermodel.Sheet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author valdemar.arantes
 */
class SwapSupplData {

    private static final Logger log = LoggerFactory.getLogger(SwapSupplData.class);
    public static final int SIZE = 8;
    private BalForSubAcctBrData.Swap swap;
    private final Sheet sheet;
    private final int sheetLine;
    private final int sheetCol;

    public SwapSupplData(Sheet sheet, int sheetLine, int sheetCol) {
        this.sheet = sheet;
        this.sheetLine = sheetLine;
        this.sheetCol = sheetCol;

        if (!ExcelUtils.isBlank(sheet, sheetLine, sheetCol, sheetCol + 8)) {
            swap = new BalForSubAcctBrData.Swap();
        }
    }

    public BalForSubAcctBrData.Swap getSwap() {
        // Não há dados na linha e nas colunas da planilha relativas ao SupplementaryData
        if (swap == null) {
            return null;
        }

        populate();

        return swap;
    }

    /**
     * Popula o objeto swap com os dados da planilha
     */
    private void populate() {
        //TODO Corrigir - por conta da alteração do XSD dos Dados Suplementares
        /*

        // Guarantee <Guar> [1..1]
        int actualCol = sheetCol;
        JaxbBeanFiller.fill(sheet, sheetLine, actualCol, "cd", swap, "guar");

        // <Position> [0..1]
        actualCol++;
        if (!ExcelUtils.isBlank(sheet, sheetLine, actualCol, actualCol + 6)) {
            int positionCol = actualCol;
            BalForSubAcctBrData.Swap.Position position = new BalForSubAcctBrData.Swap.Position();
            swap.setPosition(position);

            // Tp/Cd
            String excelData = ExcelUtils.getCellAsString(sheet, sheetLine, positionCol);
            if (StringUtils.isNotBlank(excelData)) {
                position.setTp(new BalForSubAcctBrData.Swap.Position.Tp());
                position.getTp().setCd(excelData);
            }

            // Rate
            positionCol++;
            excelData = ExcelUtils.getCellAsString(sheet, sheetLine, positionCol);
            if (StringUtils.isNotBlank(excelData)) {
                try {
                    position.setRate(Double.parseDouble(excelData));
                } catch (Exception e) {
                    String err = String.format("Erro ao converter %1s para Double "
                            + "[linha %2s, col %3s]", excelData, sheetLine, positionCol);
                    log.error(err, e);
                }
            }

            // Index
            positionCol++;
            if (!ExcelUtils.isBlank(sheet, sheetLine, actualCol, actualCol + 4)) {
                position.setIndex(new BalForSubAcctBrData.Swap.Position.Index());
                position.getIndex().setPrtry(new PrtrySupplData(sheet, sheetLine, actualCol).getPrtry());
            }
        } // Fim de Position

        // StartValueDate <StartValDt> [1..1]
        actualCol += 6;
        String excelData = ExcelUtils.getCellAsString(sheet, sheetLine, actualCol);
        if (StringUtils.isNotBlank(excelData)) {
            try {
                BeanUtils.setProperty(swap, "startValDt", excelData);
            } catch (IllegalAccessException | InvocationTargetException | RuntimeException e) {
                String err = String.format("Erro na linha %1s, coluna %2s", sheetLine, actualCol);
                log.error(err, e);
            }
        }*/

    }

}
