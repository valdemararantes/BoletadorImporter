/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.galgo.boletador.importers.util;

import br.com.galgo.boletador.importers.ExcelUtils;
import iso.std.iso._20022.tech.xsd.semt_003_001.AdditionalBalanceInformation6;
import iso.std.iso._20022.tech.xsd.semt_003_001.FinancialInstrumentQuantity1Choice;
import iso.std.iso._20022.tech.xsd.semt_003_001.SecuritiesBalanceType12Code;
import iso.std.iso._20022.tech.xsd.semt_003_001.SecuritiesBalanceType7Code;
import iso.std.iso._20022.tech.xsd.semt_003_001.SubBalanceInformation6;
import iso.std.iso._20022.tech.xsd.semt_003_001.SubBalanceQuantity3Choice;
import iso.std.iso._20022.tech.xsd.semt_003_001.SubBalanceType5Choice;
import iso.std.iso._20022.tech.xsd.semt_003_001.SubBalanceType6Choice;
import java.math.BigDecimal;
import java.util.List;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.ss.usermodel.Sheet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Instancia a classe SubBalanceInformation6 e preenche com os dados importados da planilha
 *
 * Os dados nas colunas devem ser:
 * <ul>
 * <li>SubBalanceType/Code - Código com caracteres
 * <li>Quantity - Valor
 * <li>SubBalanceAdditionalDetails - Texto
 * <li>AdditionalBalanceBreakdownDetails/SubBalanceType/Code - Código com caracteres
 * <li>AdditionalBalanceBreakdownDetails/Quantity/FaceAmount - Valor
 * </ul>
 *
 * @author valdemar.arantes
 */
public class SubBalanceInformation6Importer {

    private static final Logger log = LoggerFactory.getLogger(SubBalanceInformation6Importer.class);

    public static SubBalanceInformation6 build(Sheet sheet, int contPrice, int balBrkDwnCol) {

        log.info("Importando SubBalanceInformation a partir da célula [{}, {}]", contPrice,
                balBrkDwnCol);

        SubBalanceInformation6 retValue = new SubBalanceInformation6();

        int offset = 0;
        String subBalanceType_Code = ExcelUtils.getCellAsString(sheet, contPrice, balBrkDwnCol
                + offset++);

        // Se o código é nulo, então não há SubBalanceInformation6 definido nesta linha
        if (StringUtils.isBlank(subBalanceType_Code)) {
            return null;
        }
        String strQtyFaceAmt = ExcelUtils.getCellAsString(sheet, contPrice, balBrkDwnCol + offset++);
        String details = ExcelUtils.getCellAsString(sheet, contPrice, balBrkDwnCol + offset++);
        String addBal_subBal_Code = ExcelUtils.getCellAsString(sheet, contPrice, balBrkDwnCol
                + offset++);
        String addBal_Qty_FaceAmt = ExcelUtils.getCellAsString(sheet, contPrice, balBrkDwnCol
                + offset++);

        // SubBalanceType/Code
        try {
            SubBalanceType5Choice subBalanceType = new SubBalanceType5Choice();
            subBalanceType.setCd(SecuritiesBalanceType12Code.fromValue(subBalanceType_Code.trim().
                    toUpperCase()));
            retValue.setSubBalTp(subBalanceType);
        } catch (Exception e) {
            log.error(null, e);
        }

        // Quantity
        retValue.setQty(buildSubBalanceQuantity3Choice(strQtyFaceAmt));

        // SubBalanceAdditionalDetails
        retValue.setSubBalAddtlDtls(details);

        // AdditionalBalanceBreakdownDetails/SubBalanceType/Code - Código com caracteres
        if (StringUtils.isNotBlank(addBal_subBal_Code)) {
            AdditionalBalanceInformation6 addtlBalBrkdwnDtls = new AdditionalBalanceInformation6();
            boolean addtlBalBrkdwnDtlsAdded = false;
            try {
                SubBalanceType6Choice subBalanceTp = new SubBalanceType6Choice();
                addtlBalBrkdwnDtls.setSubBalTp(subBalanceTp);
                subBalanceTp.setCd(SecuritiesBalanceType7Code.valueOf(addBal_subBal_Code.trim().
                        toUpperCase()));
                retValue.getAddtlBalBrkdwnDtls().add(addtlBalBrkdwnDtls);
                addtlBalBrkdwnDtlsAdded = true;
            } catch (Exception e) {
                log.error(null, e);
            }

            // AdditionalBalanceBreakdownDetails/Quantity/FaceAmount
            addtlBalBrkdwnDtls.setQty(buildSubBalanceQuantity3Choice(addBal_Qty_FaceAmt));
            if (!addtlBalBrkdwnDtlsAdded) {
                retValue.getAddtlBalBrkdwnDtls().add(addtlBalBrkdwnDtls);
            }
        }

        return retValue;
    }

    private static SubBalanceQuantity3Choice buildSubBalanceQuantity3Choice(String faceAmt) {
        try {
            log.debug("faceAmt={}", faceAmt);
            SubBalanceQuantity3Choice subBalQty = new SubBalanceQuantity3Choice();
            FinancialInstrumentQuantity1Choice financInstrQty = new FinancialInstrumentQuantity1Choice();
            subBalQty.setQty(financInstrQty);
            financInstrQty.setFaceAmt(new BigDecimal(faceAmt));
            return subBalQty;
        } catch (Exception e) {
            log.error(null, e);
            return null;
        }
    }
}
