/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.galgo.boletador.importers.converter;

import com.galgo.utils.XMLGregorianCalendarConversionUtil;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

/**
 *
 * @author joyce.oliveira
 */
public class StringToXMLGregorianCalendarConverter {

    public static XMLGregorianCalendar stringToXMLGregorianCalendarAno(String s)
            throws ParseException,
            DatatypeConfigurationException {
        Date date;
        SimpleDateFormat simpleDateFormat;
        GregorianCalendar gregorianCalendar;
        simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        date = simpleDateFormat.parse(s);

        return XMLGregorianCalendarConversionUtil.asXMLGregorianCalendar_Date(date);
    }

    public static XMLGregorianCalendar stringToXMLGregorianCalendarTimeZone(String s)
            throws ParseException,
            DatatypeConfigurationException {
        XMLGregorianCalendar result = null;
        Date date;
        SimpleDateFormat simpleDateFormat;
        GregorianCalendar gregorianCalendar;
        simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        date = simpleDateFormat.parse(s);
        gregorianCalendar
                = (GregorianCalendar) GregorianCalendar.getInstance();
        gregorianCalendar.setTime(date);

        result = DatatypeFactory.newInstance().newXMLGregorianCalendar(gregorianCalendar);
        return result;
    }
}
