/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.galgo.boletador.importers.util;

import org.apache.poi.ss.usermodel.Sheet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author valdemar.arantes
 */
class PrtrySupplData {

    private static final Logger log = LoggerFactory.getLogger(PrtrySupplData.class);

    //TODO Corrigir - por conta da alteração do XSD dos Dados Suplementares
    //private BalForSubAcctBrData.Swap.Position.Index.Prtry prtry;

    private final Sheet sheet;
    private final int sheetLine;
    private final int sheetCol;

    PrtrySupplData(Sheet sheet, int sheetLine, int sheetCol) {
        this.sheet = sheet;
        this.sheetLine = sheetLine;
        this.sheetCol = sheetCol;

        //TODO Corrigir - por conta da alteração do XSD dos Dados Suplementares
        /*if (ExcelUtils.isBlank(sheet, sheetLine, sheetCol, sheetCol + 3)) {
            prtry = new BalForSubAcctBrData.Swap.Position.Index.Prtry();
        }*/
    }

    //TODO Corrigir - por conta da alteração do XSD dos Dados Suplementares
    /*public BalForSubAcctBrData.Swap.Position.Index.Prtry getPrtry() {
        if (prtry == null) {
            return null;
        }

        populate();
        return prtry;
    }*/

    private void populate() {
        //TODO Corrigir - por conta da alteração do XSD dos Dados Suplementares
        /*

        // Id
        int actualCol = sheetCol;
        JaxbBeanFiller.fill(sheet, sheetLine, actualCol, null, prtry, "id");

        // Issr
        actualCol++;
        JaxbBeanFiller.fill(sheet, sheetLine, actualCol, null, prtry, "issr");

        // SchmeNm
        actualCol++;
        JaxbBeanFiller.fill(sheet, sheetLine, actualCol, null, prtry, "schmeNm");
        */
    }
}
