/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.galgo.boletador.importers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Classe utilizada para controlar qual a próxima linha a ser utilizada para ler o próximo ativo
 *
 * @author valdemar.arantes
 */
public class AtivoSheetLineController {

    private static final Logger log = LoggerFactory.getLogger(AtivoSheetLineController.class);
    private int nextAtivoLine = 0;

    public AtivoSheetLineController(int nextAtivoLine) {
        log.debug("Nova instância com nextAtivoLine = {}", nextAtivoLine);
        this.nextAtivoLine = nextAtivoLine;
    }

    /**
     * Atualiza a próxima linha do ativo se for maior do que a já registrada nesta instância
     *
     * @param nextAtivoLine
     */
    public void updateNextAtivoLine(int nextAtivoLine) {
        if (nextAtivoLine > this.nextAtivoLine) {
            log.debug("Contador atualizado para {}", nextAtivoLine);
            this.nextAtivoLine = nextAtivoLine;
        }
    }

    public int getNextAtivoLine() {
        return nextAtivoLine;
    }

}
