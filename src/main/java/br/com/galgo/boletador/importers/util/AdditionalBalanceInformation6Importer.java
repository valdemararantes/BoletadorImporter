/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.galgo.boletador.importers.util;

import br.com.galgo.boletador.importers.ExcelUtils;
import iso.std.iso._20022.tech.xsd.semt_003_001.AdditionalBalanceInformation6;
import iso.std.iso._20022.tech.xsd.semt_003_001.FinancialInstrumentQuantity1Choice;
import iso.std.iso._20022.tech.xsd.semt_003_001.SecuritiesBalanceType7Code;
import iso.std.iso._20022.tech.xsd.semt_003_001.SubBalanceQuantity3Choice;
import iso.std.iso._20022.tech.xsd.semt_003_001.SubBalanceType6Choice;
import java.math.BigDecimal;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.ss.usermodel.Sheet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author valdemar.arantes
 */
public class AdditionalBalanceInformation6Importer {

    private static final Logger log = LoggerFactory.getLogger(
            AdditionalBalanceInformation6Importer.class);

    public static AdditionalBalanceInformation6 build(Sheet sheet, int contAddtlBalBrkdwn,
            int addtlBalBrkdwnCol) {
        int offset = 0;
        String subBalTp_Cd = ExcelUtils.getCellAsString(sheet, contAddtlBalBrkdwn, addtlBalBrkdwnCol
                + offset++);
        String qty_Unit = ExcelUtils.getCellAsString(sheet, contAddtlBalBrkdwn, addtlBalBrkdwnCol
                + offset++);
        String qty_FaceAmt = ExcelUtils.getCellAsString(sheet, contAddtlBalBrkdwn, addtlBalBrkdwnCol
                + offset++);
        log.debug("subBalTp_Cd={}, qty_Unit={}, qty_FaceAmt={}", subBalTp_Cd, qty_Unit, qty_FaceAmt);
        if (StringUtils.isBlank(subBalTp_Cd)) {
            return null;
        }

        AdditionalBalanceInformation6 addBalBrkdwn = new AdditionalBalanceInformation6();

        // Tipo de Subsaldo - SubBalanceType <SubBalTp> [1..1]
        try {
            SubBalanceType6Choice subBalTp = new SubBalanceType6Choice();
            subBalTp.setCd(SecuritiesBalanceType7Code.valueOf(subBalTp_Cd));
            addBalBrkdwn.setSubBalTp(subBalTp);
        } catch (Exception e) {
            log.error(null, e);
        }

        // Quantidade - Quantity/Quantity <Qty>/<Qty>
        try {
            SubBalanceQuantity3Choice qty = new SubBalanceQuantity3Choice();
            FinancialInstrumentQuantity1Choice qty_qty
                    = new FinancialInstrumentQuantity1Choice();
            qty.setQty(qty_qty);
            if (StringUtils.isNotBlank(qty_Unit)) {
                // SET Unit <Unit> [1..1] OU
                qty_qty.setUnit(new BigDecimal(qty_Unit));
            } else if (StringUtils.isNotBlank(qty_FaceAmt)) {
                // SET Valor de Face - FaceAmount <FaceAmt> [1..1]
                qty_qty.setFaceAmt(new BigDecimal(qty_FaceAmt));
            }
            addBalBrkdwn.setQty(qty);
        } catch (Exception e) {
            log.error(null, e);
        }

        return addBalBrkdwn;
    }

}
