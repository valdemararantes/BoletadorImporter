/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.galgo.boletador.importers.util;

import iso.std.iso._20022.tech.xsd.semt_003_001.ActiveOrHistoricCurrencyAndAmount;
import iso.std.iso._20022.tech.xsd.semt_003_001.AmountAndDirection6;
import iso.std.iso._20022.tech.xsd.semt_003_001.BalanceAmounts1;
import java.math.BigDecimal;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author joyce.oliveira
 */
public class AccountBaseCurrencyAmounts {

    private static final Logger log = LoggerFactory.getLogger(AccountBaseCurrencyAmounts.class);

    /**
     *
     * @param valorFinanceiro
     * @param valor
     * @param moeda
     * @param sinal
     * @param ganhoPerdaNRealizado
     * @param moedaGanhosPerdasNRealidos
     * @param sinalGanhoPerdaNRealizado
     */
    public void geraAccountBaseCurrencyAmounts(BalanceAmounts1 valorFinanceiro, String valor,
	    String moeda, String sinal, String ganhoPerdaNRealizado,
	    String moedaGanhosPerdasNRealidos,
	    String sinalGanhoPerdaNRealizado) {

	log.trace("valorFinanceiro={}; valor={}; moeda={}; sinal={}; ganhoPerdaNRealizado={}",
	        new Object[] { valorFinanceiro, valor, moeda, sinal, ganhoPerdaNRealizado });

	AmountAndDirection6 hldgVal = valorFinanceiro.getHldgVal();
	if (hldgVal == null) {
	    hldgVal = new AmountAndDirection6();
	    valorFinanceiro.setHldgVal(hldgVal);
	}

	ActiveOrHistoricCurrencyAndAmount vlrSaldo = new ActiveOrHistoricCurrencyAndAmount();
	hldgVal.setAmt(vlrSaldo);

	if (moeda != null) {
	    vlrSaldo.setCcy(moeda);
	}

	if (valor != null) {
	    BigDecimal valorSaldo = new BigDecimal(valor);
	    vlrSaldo.setValue(valorSaldo);
	}

	if (sinal != null) {
	    hldgVal.setSgn(Boolean.parseBoolean(sinal));
	}

	AmountAndDirection6 urlsdGnLoss = valorFinanceiro.getUrlsdGnLoss();
	if (urlsdGnLoss == null) {
	    urlsdGnLoss = new AmountAndDirection6();
	    valorFinanceiro.setUrlsdGnLoss(hldgVal);
	}

	ActiveOrHistoricCurrencyAndAmount amt = urlsdGnLoss.getAmt();
	if (amt == null) {
	    amt = new ActiveOrHistoricCurrencyAndAmount();
	    urlsdGnLoss.setAmt(amt);
	}

	if (moedaGanhosPerdasNRealidos != null) {
	    amt.setCcy(moedaGanhosPerdasNRealidos);
	}

	if (ganhoPerdaNRealizado != null) {
	    BigDecimal vlrGanhosPerdasNRealidosBigDecimal = new BigDecimal(ganhoPerdaNRealizado);
	    amt.setValue(vlrGanhosPerdasNRealidosBigDecimal);
	}

	if (sinalGanhoPerdaNRealizado != null) {
	    urlsdGnLoss.setSgn(Boolean.parseBoolean(sinalGanhoPerdaNRealizado));
	}

    }
}
