/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.galgo.boletador.importers.util;

import iso.std.iso._20022.tech.xsd.semt_003_001.GenericIdentification1;
import iso.std.iso._20022.tech.xsd.semt_003_001.Number2Choice;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author joyce.oliveira
 */
public class CpnAttchdNb {

    private static final Logger log = LoggerFactory.getLogger(CpnAttchdNb.class);

    public void gerarCpnAttchdNb(Number2Choice anexoCupom, String codigo, String id, String nome,
            String emissor) {

        log.trace("codigo={}; id={}; nome={}; emissor={}", new Object[]{codigo, id, nome, emissor});
        if (StringUtils.isBlank(codigo)) {
            log.info("O Código do Número Anexado do Cupom não foi preenchido");
            return;
        }

        //Se código for igual a LONG deve criar bloco LONG
        if ("LONG".equals(codigo)) {
            GenericIdentification1 lng = anexoCupom.getLng();
            if (lng == null) {
                lng = new GenericIdentification1();
                anexoCupom.setLng(lng);
            }
            lng.setId(id);
            lng.setIssr(emissor);
            lng.setSchmeNm(nome);
        } else {
            // Como o codigo != LONG, o valor será importado se for uma string com
            // 3 dígitos (Tomador ou SHORT)
            if (codigo.trim().length() == 3 & StringUtils.isNumeric(codigo)) {
                anexoCupom.setShrt(codigo);
            } else {
                log.info("O Código do Número Anexado do Cupom não foi preenchido com 3 dígitos.");
            }
        }

    }
}
