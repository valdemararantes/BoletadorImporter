/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.galgo.boletador.importers.util;

import iso.std.iso._20022.tech.xsd.semt_003_001.AdditionalBalanceInformation6;
import iso.std.iso._20022.tech.xsd.semt_003_001.FinancialInstrumentQuantity1Choice;
import iso.std.iso._20022.tech.xsd.semt_003_001.SubBalanceQuantity3Choice;
import java.math.BigDecimal;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author joyce.oliveira
 */
public class AdditionalBalanceBreakdownDetails {

    private static final Logger log = LoggerFactory.getLogger(
            AdditionalBalanceBreakdownDetails.class);

    /**
     *
     * @param complQntd
     * @param valor
     */
    public void geraAdditionalBalanceBreakdownDetails(AdditionalBalanceInformation6 complQntd,
            String valor) {

        log.trace("valor={}", valor);

        SubBalanceQuantity3Choice qty = complQntd.getQty();
        if (qty == null) {
            qty = new SubBalanceQuantity3Choice();
            complQntd.setQty(qty);
        }

        FinancialInstrumentQuantity1Choice qtyII = qty.getQty();
        if (qtyII == null) {
            qtyII = new FinancialInstrumentQuantity1Choice();
            qty.setQty(qtyII);
        }

        if (valor != null) {
            BigDecimal vlr = new BigDecimal(valor);
            qtyII.setFaceAmt(vlr);
        }
    }
}
