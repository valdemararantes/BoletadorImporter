/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.galgo.boletador.pot;

import com.galgo.utils.ClassUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.SystemUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author valdemar.arantes
 */
public class ClassloaderPOT {

    private static final Logger log = LoggerFactory.getLogger(ClassloaderPOT.class);

    public static void main(String... args) {
        log.debug("************** Início *************");
        try {
            ClassUtils.isListField(String.class, "aaa");

            String[] cpList = StringUtils.split(SystemUtils.JAVA_CLASS_PATH, ";");
            for (String cp : cpList) {
                System.out.println(cp);
            }

        } catch (Exception e) {
            log.error(null, e);
        } finally {
            log.debug("**************** Fim ***************");
        }
    }
}
