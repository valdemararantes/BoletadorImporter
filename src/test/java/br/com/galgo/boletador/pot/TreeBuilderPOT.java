/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.galgo.boletador.pot;

import br.com.galgo.boletador.importers.util.Tree;
import br.com.galgo.boletador.importers.util.TreeBuilder;
import iso.std.iso._20022.tech.xsd.semt_003_001.SubBalanceInformation6;
import java.io.FileOutputStream;
import java.util.List;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author valdemar
 */
public class TreeBuilderPOT {

    private static final Logger log = LoggerFactory.getLogger(TreeBuilderPOT.class);

    public static final void main(String... args) {
        testGenericClass(SubBalanceInformation6.class, "balBrkdwn");
    }

    private static void testGenericClass(Class<?> beanClass, String beanName) {
        TreeBuilder treeBuilder = new TreeBuilder(beanClass, beanName);
        Tree<TreeBuilder.Node> tree = treeBuilder.buildTree();

        List<TreeBuilder.Node> leaves = tree.getLeaves(tree.getHead());
        log.debug("{} folhas encontradas na árvore", leaves.size());

        try {
            // Logando todas as propriedades aninhadas das folhas da árvores de classes
            Workbook wb = new XSSFWorkbook();
            Sheet sheet = wb.createSheet();
            int i = 0;
            for (TreeBuilder.Node leaf : leaves) {
                String nestePropertyName = treeBuilder.getNestePropertyName(leaf);
                log.debug("NestedPropertyName de {} | {}", leaf, nestePropertyName);
                Row row = sheet.createRow(i);
                i++;
                row.createCell(0).setCellValue(leaf.propName);
                row.createCell(1).setCellValue(leaf.propClass.getName());
                row.createCell(2).setCellValue(nestePropertyName);
            }
            String excelFilename = String.format("../BoletadorImporter/nestedProperties_%1s.xlsx",
                    beanName);
            wb.write(new FileOutputStream(excelFilename));
        } catch (Exception e) {
            log.error(null, e);
        }
    }
}
