/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.galgo.boletador.importers;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author valdemar.arantes
 */
public class ExcelUtilsTest {

    private static final Logger log = LoggerFactory.getLogger(ExcelUtilsTest.class);
    private Workbook wb;

    @Before public void prepare() throws IOException {
        String xlsName = "/in_1.xls";
        log.info("Abrindo arquivo {}", getClass().getResource(xlsName));
        wb = new HSSFWorkbook(new BufferedInputStream(this.getClass().getResourceAsStream(xlsName)));
    }

    @Test public void getCellAsString() {
        Sheet sheet = wb.getSheetAt(0);
        String str = ExcelUtils.getCellAsString(sheet, 3, 0);
        Assert.assertNull(str);

        try {
            ExcelUtils.getCellAsString(sheet, 5, 0);
            Assert.fail("Deveria ter lançado a exceção "
                    + "br.com.galgo.boletador.importers.NonExistentRowException");
        } catch (NonExistentRowException e) {
            log.error("\ne.getMessage={}\ne.toString={}", e.getMessage(), e.toString());
        } catch (Exception e) {
            log.error(null, e);
            Assert.fail("Esta exceção não deveria ter ocorrido");
        }
    }

    @Test public void getCellsAsListOfMap() {
        Sheet sheet = wb.getSheetAt(0);
        List<Map<String, String>> listOfMap = ExcelUtils.getCellsAsListOfMap(sheet, 1, 2, 5);
        Assert.assertEquals("00000000000272", listOfMap.get(0).get("Fr_OrgId_Id"));
        Assert.assertEquals(3, listOfMap.size());
        Assert.assertEquals("enissor próprio", listOfMap.get(2).get("Fr_OrgId_Issuer"));
    }

}
