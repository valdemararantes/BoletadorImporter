/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.galgo.boletador.importers.util;

import br.anbimacom.balanceforsubaccountbrazil.BalForSubAcctBrData;
import br.anbimacom.balanceforsubaccountbrazil.BalForSubAcctBrData.RealStatePrtfl.*;
import br.com.galgo.boletador.Printer;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.beanutils.expression.DefaultResolver;
import org.apache.commons.beanutils.expression.Resolver;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.OfficeXmlFileException;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author valdemar.arantes
 */
public class SupplementaryDataTest {

    private static Logger log = LoggerFactory.getLogger(SupplementaryDataTest.class);
    private static Workbook wb;
    private static Sheet sheet;
    private static final int SUPPL_DATA_FIRST_COL = 58;

    public SupplementaryDataTest() {
    }

    @BeforeClass
    public static void setUpClass() {
        log.debug("**********************************");
        String wbName = "boletador_template5.xls";
        log.info("Abrindo arquivo {}", wbName);
        File xlsFileName = new File(wbName);

        InputStream wbInpStream = null;
        try {
            wbInpStream = new FileInputStream(xlsFileName);
            wb = new HSSFWorkbook(wbInpStream);
        } catch (OfficeXmlFileException e) {
            log.info(e.getMessage());
            log.info("Tentando abrir como se fosse um XLSX...");
            try {
                wb = new XSSFWorkbook(wbInpStream);
                log.info("Arquivo aberto com sucesso!");
            } catch (IOException e1) {
                log.error(null, e1);
                Assert.fail(e.getMessage());
            }
        } catch (Exception e) {
            log.error(null, e);
            Assert.fail(e.getMessage());
        }

        sheet = wb.getSheet("Ativos");
        log.info("setUpClass OK");
    }

    @Test
    public void BalNotNull() {
        log.info("Início");
        try {
            SupplementaryData supplData = new SupplementaryData(sheet, 3, SUPPL_DATA_FIRST_COL);
            BalForSubAcctBrData balForSubAcctBrData = supplData.getBalForSubAcctBrData();
            Assert.assertNotNull(balForSubAcctBrData);
            Printer.printAsXml(balForSubAcctBrData);
        } catch (Throwable e) {
            log.error(null, e);
            Assert.fail(e.getMessage() == null ? e.toString() : e.getMessage());
        } finally {
            log.info("Fim do método");
        }
    }

    //@Test
    public void BalNull() {
        try {
            SupplementaryData supplData = new SupplementaryData(sheet, 4, SUPPL_DATA_FIRST_COL);
            BalForSubAcctBrData balForSubAcctBrData = supplData.getBalForSubAcctBrData();
            Assert.assertNull(balForSubAcctBrData);
        } catch (Exception e) {
            Assert.fail(e.getMessage() == null ? e.toString() : e.getMessage());
            log.error(null, e);
        } finally {
            log.info("Fim do método");
        }
    }

    @Test
    public void a() {
        String nestedProp = "aaa.bbb.ccc";
        Resolver resolver = new DefaultResolver();
        log.debug("resolver.next({})={}", nestedProp, resolver.next(nestedProp));
        log.debug("resolver.remove({})={}", nestedProp, resolver.remove(nestedProp));

        BalForSubAcctBrData.RealStatePrtfl.ValtInf.Prtry prtry = new BalForSubAcctBrData.RealStatePrtfl.ValtInf.Prtry();
        prtry.setId("id_");
        prtry.setIssr("issr_");
        prtry.setSchmeNm("schmeNm_");
        log.debug(ToStringBuilder.reflectionToString(prtry));

        try {
            Map m = new HashMap();
            m.put("id", new Date());
            BeanUtils.copyProperties(prtry, m);
            log.debug(ToStringBuilder.reflectionToString(prtry));
        } catch (IllegalAccessException | InvocationTargetException ex) {
            log.error(null, ex);
        }
    }

    @Test
    public void createLease() {
        if (false) {
            Lease l = new Lease();
            l.getTp().setCd("");
            l.setAmt(BigDecimal.ZERO);
        }

        Tree<String> lease = new Tree<String>("lease");
        lease.addLeaf("tp", "cd");
        lease.addLeaf("amt", "value").getParent().addLeaf("ccy", "a").getParent().addLeaf("b");
        log.debug("Tree: {}", lease);
        log.debug("Successors: {}", lease.getLeaves(lease.getHead()));

        int line = 3, col = 89;

    }

}


