/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.galgo.boletador.importers;

import br.com.galgo.boletador.Printer;
import com.sistemagalgo.schemaposicaoativos.GalgoAssBalStmtComplexType;
import java.io.InputStream;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author valdemar.arantes
 */
public class WorkbookImporterTest {

    private static final Logger log = LoggerFactory.getLogger(WorkbookImporterTest.class);

    public void importWorkbookTest() {
        try {
            log.debug("**********************************");
            InputStream wbInpStream = this.getClass().getClassLoader().getResourceAsStream(
                    "in_1.xls");
            Assert.assertNotNull("Arquivo in_1.xls não foi encontrado", wbInpStream);
            GalgoAssBalStmtComplexType glg = new WorkbookImporter(wbInpStream).importWorkbook();
            Assert.assertNotNull("Objeto GalgoAssBalStmtComplexType nulo", glg);
            Printer.printAsXml(glg);
        } catch (Exception e) {
            log.error(null, e);
            Assert.fail();
        }
    }

    @Test public void importWorkbookTestAtivo() {
        try {
            log.debug("**********************************");
            InputStream wbInpStream = this.getClass().getClassLoader().getResourceAsStream(
                    "in_2.xls");
            Assert.assertNotNull("Arquivo in_2.xls não foi encontrado", wbInpStream);
            GalgoAssBalStmtComplexType glg = new WorkbookImporter(wbInpStream).importWorkbook();
            Printer.printAsXml(glg);
        } catch (Exception e) {
            log.error(null, e);
            Assert.fail();
        }
    }
}
