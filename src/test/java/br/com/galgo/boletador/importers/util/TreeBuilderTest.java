/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.galgo.boletador.importers.util;

import br.anbimacom.balanceforsubaccountbrazil.BalForSubAcctBrData;
import java.util.List;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author valdemar.arantes
 */
public class TreeBuilderTest {

    private static final Logger log = LoggerFactory.getLogger(TreeBuilderTest.class);

    public TreeBuilderTest() {
    }

    @Test
    public void test() {
        TreeBuilder treeBuilder = new TreeBuilder(BalForSubAcctBrData.class, "BalForSubAcctBrData");
        Tree<TreeBuilder.Node> tree = treeBuilder.buildTree();

        List<TreeBuilder.Node> leaves = tree.getLeaves(tree.getHead());
        log.debug("{} folhas encontradas na árvore", leaves.size());
        Assert.assertEquals(80, leaves.size());
    }
}
