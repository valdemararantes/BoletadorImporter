/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.galgo.boletador.importers;

import br.com.galgo.boletador.Printer;
import com.sistemagalgo.schemaposicaoativos.GalgoAssBalStmtComplexType;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.OfficeXmlFileException;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author valdemar.arantes
 */
public class FundoImporterTest {

    private static Logger log = LoggerFactory.getLogger(FundoImporterTest.class);
    private static Workbook wb;

    @BeforeClass public static void before() throws IOException {
        log.debug("**********************************");
        String wbName = "boletador_template5.xls";
        log.info("Abrindo arquivo {}", wbName);
        File xlsFile = new File(wbName);
        Assert.assertTrue(xlsFile.exists() && xlsFile.isFile());
        InputStream wbInpStream = new BufferedInputStream(new FileInputStream(xlsFile));
        try {
            wb = new HSSFWorkbook(wbInpStream);
        } catch (OfficeXmlFileException e) {
            wb = new XSSFWorkbook(wbInpStream);
        }
    }

    @Test public void importSheet() {
        GalgoAssBalStmtComplexType glg = new GalgoAssBalStmtComplexType();
        new FundoImporter(glg, wb.getSheet("Fundo")).importSheet();
        new AtivosImporter(glg, wb.getSheet("Ativos")).importSheet();

        Printer.print(glg);
        Printer.printAsXml(glg);
    }
}
