/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.galgo.boletador;

import java.io.StringWriter;
import javax.xml.bind.JAXB;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectWriter;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author valdemar.arantes
 */
public class Printer {

    private static final Logger log = LoggerFactory.getLogger(Printer.class);

    /**
     * Loga o objeto no formato Json
     * @param jaxbObj 
     */
    public static void print(Object jaxbObj) {
        ObjectMapper mapper = new ObjectMapper();
        mapper.setSerializationInclusion(JsonSerialize.Inclusion.NON_NULL);
        StringWriter writer = new StringWriter();
        ObjectWriter writerWithDefaultPrettyPrinter = mapper.writerWithDefaultPrettyPrinter();
        try {
            writerWithDefaultPrettyPrinter.writeValue(writer, jaxbObj);
            String jsonString = writer.getBuffer().toString();
            log.info(jsonString);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    public static void printAsXml(Object jaxbObj) {
            StringWriter w = new StringWriter();
            JAXB.marshal(jaxbObj, w);
            log.debug(w.toString());
    }

    /**
     * Coerces the JAXB marshaller to declare the "xsi" and "xsd" namespaces at the root element
     * instead of putting them inline on each element that uses one of the namespaces.
     */
//    private static class CustomNamespacePrefixMapper extends NamespacePrefixMapper {
//
//        @Override
//        public String getPreferredPrefix(String namespaceUri, String suggestion,
//                boolean requirePrefix) {
//            return suggestion;
//        }
//
//        @Override
//        public String[] getPreDeclaredNamespaceUris2() {
//            return new String[]{
//                "xsi", "http://www.w3.org/2001/XMLSchema-instance",
//                "xsd", "http://www.w3.org/2001/XMLSchema"
//            };
//
//        }
//    }
}
